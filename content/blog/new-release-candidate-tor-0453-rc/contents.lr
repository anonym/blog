title: New release candidate: Tor 0.4.5.3-rc
---
pub_date: 2021-01-12
---
author: nickm
---
tags: release candidate
---
categories: releases
---
_html_body:

<p>There's a new release candidate available for download. If you build Tor from source, you can download the source code for 0.4.5.3-rc from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release in 3-4 days.</p>
<p>We're getting closer and closer to stable here, so I hope that people will try this one out and report any bugs they find.</p>
<p>Tor 0.4.5.3-rc is the first release candidate in its series. It fixes several bugs, including one that broke onion services on certain older ARM CPUs, and another that made v3 onion services less reliable.</p>
<p>Though we anticipate that we'll be doing a bit more clean-up between now and the stable release, we expect that our remaining changes will be fairly simple. There will be at least one more release candidate before 0.4.5.x is stable.</p>
<h2>Changes in version 0.4.5.3-rc - 2021-01-12</h2>
<ul>
<li>Major bugfixes (onion service v3):
<ul>
<li>Stop requiring a live consensus for v3 clients and services, and allow a "reasonably live" consensus instead. This allows v3 onion services to work even if the authorities fail to generate a consensus for more than 2 hours in a row. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40237">40237</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor features (crypto):
<ul>
<li>Fix undefined behavior on our Keccak library. The bug only appeared on platforms with 32-byte CPU cache lines (e.g. armv5tel) and would result in wrong digests. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40210">40210</a>; bugfix on 0.2.8.1-alpha. Thanks to Bernhard Übelacker, Arnd Bergmann and weasel for diagnosing this.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (documentation):
<ul>
<li>Mention the "!badexit" directive that can appear in an authority's approved-routers file, and update the description of the "!invalid" directive. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40188">40188</a>.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix a compilation warning about unreachable fallthrough annotations when building with "--enable-all-bugs-are-fatal" on some compilers. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40241">40241</a>; bugfix on 0.3.5.4-alpha.</li>
<li>Fix the "--enable-static-tor" switch to properly set the "-static" compile option onto the tor binary only. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40111">40111</a>; bugfix on 0.2.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (config, bridge):
<ul>
<li>Really fix the case where torrc has a missing ClientTransportPlugin but is configured with a Bridge line and UseBridges. Previously, we didn't look at the managed proxy list and thus would fail for the "exec" case. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40106">40106</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging, relay):
<ul>
<li>Log our address as reported by the directory authorities, if none was configured or detected before. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40201">40201</a>; bugfix on 0.4.5.1-alpha.</li>
<li>When a launching bandwidth testing circuit, don't incorrectly call it a reachability test, or trigger a "CHECKING_REACHABILITY" control event. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40205">40205</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay, statistics):
<ul>
<li>Report the correct connection statistics in our extrainfo documents. Previously there was a problem in the file loading function which would wrongly truncate a state file, causing the wrong information to be reported. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40226">40226</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (SOCKS5):
<ul>
<li>Handle partial SOCKS5 messages correctly. Previously, our code would send an incorrect error message if it got a SOCKS5 request that wasn't complete. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40190">40190</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-290876"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290876" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Our name (not verified)</span> said:</p>
      <p class="date-time">January 13, 2021</p>
    </div>
    <a href="#comment-290876">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290876" class="permalink" rel="bookmark">The issue with v3 onion…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The issue with v3 onion services has been driving us crazy, so good on you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290890"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290890" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 14, 2021</p>
    </div>
    <a href="#comment-290890">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290890" class="permalink" rel="bookmark">Please focus more on Android…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please focus more on Android, its gone to hell since about last October<br />
Why not build off Brave? Its basically what Tor used to be. You can run Tor with Brave if you use a Tor friendly VPN service</p>
</div>
  </div>
</article>
<!-- Comment END -->
