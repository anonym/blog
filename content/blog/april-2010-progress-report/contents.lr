title: April 2010 Progress Report
---
pub_date: 2010-05-10
---
author: phobos
---
tags:

progress report
tor browser bundle
bug fixes
alpha release
metrics
enhancements
---
categories:

applications
metrics
releases
reports
---
_html_body:

<p><strong>New releases</strong></p>

<ul>
<li>On April 24, we released Tor 0.2.2.13-alpha. This version addresses the recent connection and memory overload problems we’ve been seeing on relays, especially relays with their DirPort open. If your relay has been crashing, or you turned it off because it used too many resources, give this release a try.
<p>o Major bugfixes:<br />
    - Teach relays to defend themselves from connection overload. Relays<br />
      now close idle circuits early if it looks like they were intended<br />
      for directory fetches. Relays are also more aggressive about closing<br />
      TLS connections that have no circuits on them. Such circuits are<br />
      unlikely to be re-used, and tens of thousands of them were piling<br />
      up at the fast relays, causing the relays to run out of sockets<br />
      and memory. Bugfix on 0.2.0.22-rc (where clients started tunneling<br />
      their directory fetches over TLS).</p>
<p>  o Minor features:<br />
    - Finally get rid of the deprecated and now harmful notion of "clique<br />
      mode", where directory authorities maintain TLS connections to<br />
      every other relay.<br />
    - Directory authorities now do an immediate reachability check as soon<br />
      as they hear about a new relay. This change should slightly reduce<br />
      the time between setting up a relay and getting listed as running<br />
      in the consensus. It should also improve the time between setting<br />
      up a bridge and seeing use by bridge users.<br />
    - Directory authorities no longer launch a TLS connection to every<br />
      relay as they startup. Now that we have 2k+ descriptors cached,<br />
      the resulting network hiccup is becoming a burden. Besides,<br />
      authorities already avoid voting about Running for the first half<br />
      hour of their uptime.
</p></li>
<li>On April 20, 2010 we released Tor 0.2.2.12-alpha.  This release fixes a critical bug in how directory authorities handle and vote on descriptors. It was causing relays to drop out of the consensus.
<p>Changes in version 0.2.2.12-alpha - 2010-04-20<br />
  o Major bugfixes:<br />
    - Many relays have been falling out of the consensus lately because<br />
      not enough authorities know about their descriptor for them to get<br />
      a majority of votes. When we deprecated the v2 directory protocol,<br />
      we got rid of the only way that v3 authorities can hear from each<br />
      other about other descriptors. Now authorities examine every v3<br />
      vote for new descriptors, and fetch them from that authority. Bugfix<br />
      on 0.2.1.23.<br />
    - Fix two typos in tor_vasprintf() that broke the compile on Windows,<br />
      and a warning in or.h related to bandwidth_weight_rule_t that<br />
      prevented clean compile on OS X. Fixes bug 1363; bugfix on<br />
      0.2.2.11-alpha.<br />
    - Fix a segfault on relays when DirReqStatistics is enabled<br />
      and 24 hours pass. Bug found by keb. Fixes bug 1365; bugfix on<br />
      0.2.2.11-alpha.</p>
<p>  o Minor bugfixes:<br />
    - Demote a confusing TLS warning that relay operators might get when<br />
      someone tries to talk to their OrPort. It is neither the operator's<br />
      fault nor can they do anything about it. Fixes bug 1364; bugfix<br />
      on 0.2.0.14-alpha.
</p></li>
<li>On April 15, we released Tor 0.2.2.11-alpha.  It fixes yet another instance of broken OpenSSL libraries that was causing some relays to drop out of the consensus.
<p> o Major bugfixes:<br />
    - Directory mirrors were fetching relay descriptors only from v2<br />
      directory authorities, rather than v3 authorities like they should.<br />
      Only 2 v2 authorities remain (compared to 7 v3 authorities), leading<br />
      to a serious bottleneck. Bugfix on 0.2.0.9-alpha. Fixes bug 1324.<br />
    - Fix a parsing error that made every possible value of<br />
      CircPriorityHalflifeMsec get treated as "1 msec". Bugfix<br />
      on 0.2.2.7-alpha. Rename CircPriorityHalflifeMsec to<br />
      CircuitPriorityHalflifeMsec, so authorities can tell newer relays<br />
      about the option without breaking older ones.<br />
    - Fix SSL renegotiation behavior on OpenSSL versions like on Centos<br />
      that claim to be earlier than 0.9.8m, but which have in reality<br />
      backported huge swaths of 0.9.8m or 0.9.8n renegotiation<br />
      behavior. Possible fix for some cases of bug 1346.</p>
<p>  o Minor features:<br />
    - Experiment with a more aggressive approach to preventing clients<br />
      from making one-hop exit streams. Exit relays who want to try it<br />
      out can set "RefuseUnknownExits 1" in their torrc, and then look<br />
      for "Attempt by %s to open a stream" log messages. Let us know<br />
      how it goes!<br />
    - Add support for statically linking zlib by specifying<br />
      --enable-static-zlib, to go with our support for statically linking<br />
      openssl and libevent. Resolves bug 1358.</p>
<p>  o Minor bugfixes:<br />
    - Fix a segfault that happens whenever a Tor client that is using<br />
      libevent2's bufferevents gets a hup signal. Bugfix on 0.2.2.5-alpha;<br />
      fixes bug 1341.<br />
    - When we cleaned up the contrib/tor-exit-notice.html file, we left<br />
      out the first line. Fixes bug 1295.<br />
    - When building the manpage from a tarball, we required asciidoc, but<br />
      the asciidoc -&gt; roff/html conversion was already done for the<br />
      tarball. Make 'make' complain only when we need asciidoc (either<br />
      because we're compiling directly from git, or because we altered<br />
      the asciidoc manpage in the tarball). Bugfix on 0.2.2.9-alpha.<br />
    - When none of the directory authorities vote on any params, Tor<br />
      segfaulted when trying to make the consensus from the votes. We<br />
      didn't trigger the bug in practice, because authorities do include<br />
      params in their votes. Bugfix on 0.2.2.10-alpha; fixes bug 1322.</p>
<p>  o Testsuite fixes:<br />
    - In the util/threads test, no longer free the test_mutex before all<br />
      worker threads have finished. Bugfix on 0.2.1.6-alpha.<br />
    - The master thread could starve the worker threads quite badly on<br />
      certain systems, causing them to run only partially in the allowed<br />
      window. This resulted in test failures. Now the master thread sleeps<br />
      occasionally for a few microseconds while the two worker-threads<br />
      compete for the mutex. Bugfix on 0.2.0.1-alpha.
</p></li>
<li>Tor Browser Bundle for GNU/Linux was updated throughout the month, from versions 1.0.1 to 1.0.4.  The details are:<br />
1.0.4: Released 2010-04-24<br />
       Update Tor to 0.2.2.13-alpha
<p>1.0.3: Released 2010-04-20<br />
       Update Tor to 0.2.2.12-alpha</p>
<p>1.0.2: Released 2010-04-18<br />
       Update Tor to 0.2.2.10-alpha<br />
       Update to Vidalia 0.2.8</p>
<p>1.0.1: Released 2010-04-08<br />
       Stop TBB from crashing when it tries to download files<br />
       Update Torbutton to 1.2.5
</p></li>
<li>On April 4, we released Tor Browser Bundle for Windows version 1.3.4.  The changes were an update to Firefox 3.5.9 to address some security issues, and update the Torbutton Firefox extension to 1.2.5.</li>
<li>On April 8, we released the latest Torbutton Firefox Extension version, 1.2.5.  This release includes a number of bugfixes and some new features.  The details are:
<p>  * bugfix: bug 1169: Fix blank popup conflict with CoolPreviews<br />
  * bugfix: bug 1246: Fix IST and other HH:30 timezone issues.<br />
  * bugfix: bug 1219: Fix the toggle warning loop issue on settings change.<br />
  * bugfix: bug 1321: Fix a session restore bug when closing the last window<br />
  * bugfix: bug 1302: Update useragent to FF3.6.3 on WinNT6.<br />
  * bugfix: bug 1157: Add logic to handle torbutton crashed state conflicts<br />
  * bugfix: bug 1235: Improve the 'changed-state' refresh warning message<br />
  * bugfix: bug 1337: Bind alert windows to correct browser window<br />
  * bugfix: bug 1055: Make the error console the default log output location<br />
  * bugfix: bug 1032: Fix an exception in the localhost proxy filter<br />
  * misc: Always tell a website our window size is rounded even if it's not<br />
  * misc: Add some suggestions to warning about loading external content<br />
  * new: Add option to always update Torbutton via Tor. On by default<br />
  * new: Redirect Google queries elsewhere on captcha (default ixquick)<br />
  * new: Strip identifying info off of Google searchbox queries</p>
</li>
<li>On April 11, we released Vidalia 0.2.8.  It contains updated translations, updates to the universal plug and play libraries, and better compatibility with newer versions of the Nokia Qt toolkit.
<p>  o Stop using our custom dock icon implementation on OS X and just use<br />
    QSystemTrayIcon everywhere. Fixes the build on Snow Leopard.<br />
    (Ticket #562)<br />
  o Update the bundled CA certificates to re-enable downloading bridges<br />
    from bridges.torproject.org via SSL.<br />
  o Include a pre-configured qt.conf file in the Mac OS X bundles that<br />
    disable Qt plugin loading from the default directories. Otherwise,<br />
    users who have Qt installed in a system-wide location would end up<br />
    loading the libraries twice and crashing.<br />
  o Include libgcc_s_dw2-1.dll in the Windows installers, since Qt 4.6 now<br />
    depends on that DLL. Including the .dll is currently hardcoded, so the<br />
    Windows installer must be built using Qt 4.6. (Ticket #555)<br />
  o Update the included version of miniupnpc to 1.4.20100407.<br />
  o Add Burmese and Thai UI translations.
</p></li>
</ul>

<p><strong>Design, develop, and implement enhancements that make Tor a better tool for users in censored countries.</strong></p>

<p>Karsten did major backend processing work on the <a href="http://metrics.torproject.org" rel="nofollow">http://metrics.torproject.org</a> data and website.  These improvements allow for an automated process to update the various graphs and analyze the data sets.  The whole system is better documented to assist someone in replicating the environment from scratch.</p>

<p><strong>Outreach and Advocacy</strong></p>

<ul>
<li>Jacob talked at SOURCE Boston about Tor, censorship circumvention, and running relays. <a href="http://www.sourceconference.com/" rel="nofollow">http://www.sourceconference.com/</a>.  Jacob's presentation can be found at <a href="https://svn.torproject.org/svn/projects/presentations/SOURCE-Boston-2010.pdf" rel="nofollow">https://svn.torproject.org/svn/projects/presentations/SOURCE-Boston-201…</a>.</li>
<li>Andrew lectured about anonymous communications and Tor at the Portland campus of the University of Southern Maine.</li>
<li>Roger lectured about anonymous communications and Tor at the Albuquerque campus of the University of New Mexico.</li>
<li>Andrew was interviewed by Radio Free Asia: Vietnam about using Tor for online privacy and anonymity.  <a href="http://www.rfa.org/vietnamese/in_depth/TOR-free-anti-censorship-tool-KhAn%20-04262010195030.html" rel="nofollow">http://www.rfa.org/vietnamese/in_depth/TOR-free-anti-censorship-tool-Kh…</a></li>
</ul>

<p><strong>Preconfigured privacy (circumvention) bundles for USB or LiveCD.</strong></p>

<p>Released new versions of Tor Browser Bundle for GNU/Linux and Windows. See above for details.</p>

<p><strong>Scalability, load balancing, directory overhead, efficiency.</strong></p>

<p>From the 0.2.2.12-alpha release notes:<br />
Directory authorities now do an immediate reachability check as soon as they hear about a new relay. This change should slightly reduce the time between setting up a relay and getting listed as running in the consensus. It should also improve the time between setting up a bridge and seeing use by bridge users.</p>

<p>In March, we changed a setting on a majority of the Directory Authorities called CircPriorityHalflifeMsec.  Based upon research at the University of Waterloo, changing this setting could improve the overall performance of the network by fine tuning the earned weighted mean average bit bucket for better performance.  Due to a bug in the CircPriorityHalflifeMsec parsing, the average latency in the network increased dramatically.  At the same time, China unblocked the list of public and bridge relays, flooding hundreds of thousands of clients back into the network. It seems the network responded poorly to both events.</p>

<p><strong>More reliable (e.g. split) download mechanism.</strong></p>

<p>The GNU/Linux tor browser bundle with localizations was added to the get-tor email auto-responder.</p>

<p>Translation work, ultimately a browser-based approach.</p>

<ul>
<li>Updated text strings for the get-tor email auto-responder to make it easier for translators to complete the work.</li>
<li>Added new translations of Vietnamese, Greek and Serbian to all projects.</li>
<li>Many updated translated strings in German, Polish, Greek, Dutch, Finnish, Russian, Japanese, Chinese, French, Burmese, Spanish, Farsi, Arabic, Swedish, Turkish, and Norwegian.</li>
</ul>

---
_comments:

<a id="comment-5646"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5646" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 12, 2010</p>
    </div>
    <a href="#comment-5646">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5646" class="permalink" rel="bookmark">And apparently users in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>And apparently users in China have gone back to limbo given tor has been having trouble finding sources to connect even while using bridges.<br />
Any light on that?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-5650"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5650" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 12, 2010</p>
    </div>
    <a href="#comment-5650">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5650" class="permalink" rel="bookmark">Not actually related to this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not actually related to this entry, but I have to say...<br />
Recently, Tor is becoming very unstable in China. The circuit seems to be established sometimes, but no data can be transfered over it. (i.e. HTTP GET request is very likely result a TIMEOUT)<br />
Not sure what's happening there...Need you help.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5679"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5679" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5650" class="permalink" rel="bookmark">Not actually related to this</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5679">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5679" class="permalink" rel="bookmark">I think the GFW might be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think the GFW might be traffic shaping the connections personally, though I can't be sure about that.  The reason I say this is the bridges I run (that haven't been blocked) have been seeing very little throughput by the users which seems strange.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-5662"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5662" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 13, 2010</p>
    </div>
    <a href="#comment-5662">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5662" class="permalink" rel="bookmark">Please see</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please see <a href="https://blog.torproject.org/blog/china-blocking-tor-round-two#comment-5639" rel="nofollow">https://blog.torproject.org/blog/china-blocking-tor-round-two#comment-5…</a>.  We are aware.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-5674"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5674" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 15, 2010</p>
    </div>
    <a href="#comment-5674">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5674" class="permalink" rel="bookmark">Looking at your metrics</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Looking at your metrics graphs,  I'd say you need a new bridge distribution strategy for China. The number of users has fallen off a cliff and is now close to zero most of the time. The GFW is winning this contest.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5676"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5676" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 15, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5674" class="permalink" rel="bookmark">Looking at your metrics</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5676">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5676" class="permalink" rel="bookmark">We agree.  We&#039;re trying a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We agree.  We're trying a new one right now, it just has a slow ramp-up period by design.  We're measuring the time duration from publishing bridges until they are blocked by the GFW.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-5680"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5680" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2010</p>
    </div>
    <a href="#comment-5680">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5680" class="permalink" rel="bookmark">just wondering if there are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>just wondering if there are plans to include auto-update for Tor/Vidalia?<br />
I have the combo running on an unattented machine to provide a relay but I worry that if it gets out-of-date it's not going to be as secure/useful.<br />
Having a daily poll-update cycle that's seamless (though on Windows UC may be an issue) or at least downloads the update and flags in the UI that it's available would be very cool</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5682"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5682" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 16, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-5680" class="permalink" rel="bookmark">just wondering if there are</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-5682">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5682" class="permalink" rel="bookmark">Yes there is the code and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes there is the code and framework for doing this already built.  We're planning a deployment of the infrastructure this summer.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-5711"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5711" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 19, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-5711">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5711" class="permalink" rel="bookmark">Please kindly help me, my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please kindly help me, my tor is not logging my yahoo messenger in, kindly send me an email to <a href="mailto:micheal4onlyu@yahoo.com" rel="nofollow">micheal4onlyu@yahoo.com</a> i am using mtn nigeria please help me...</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-5738"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5738" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 23, 2010</p>
    </div>
    <a href="#comment-5738">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5738" class="permalink" rel="bookmark">please i need a new bridge</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>please i need a new bridge for tor you can message me at ..... <a href="mailto:roseben2008@gmail.com" rel="nofollow">roseben2008@gmail.com</a> thanks. and i want you to give me the latest tor which is faster ok.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-5754"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5754" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 25, 2010</p>
    </div>
    <a href="#comment-5754">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5754" class="permalink" rel="bookmark">Has the problem been</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Has the problem been resolved that was raised in Feb 2009 by a chinaman teacher/educator was it called something like  "black hat" not sure<br />
thanks<br />
tim</p>
</div>
  </div>
</article>
<!-- Comment END -->
