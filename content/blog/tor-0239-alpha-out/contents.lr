title: Tor 0.2.3.9-alpha is out
---
pub_date: 2011-12-09
---
author: erinn
---
tags:

tor
bridges
hidden services
alpha release
ipv6
pluggable transports
---
categories:

circumvention
network
onion services
releases
---
_html_body:

<p>Tor 0.2.3.9-alpha introduces initial IPv6 support for bridges, adds<br />
  a "DisableNetwork" security feature that bundles can use to avoid<br />
  touching the network until bridges are configured, moves forward on<br />
  the pluggable transport design, fixes a flaw in the hidden service<br />
  design that unnecessarily prevented clients with wrong clocks from<br />
  reaching hidden services, and fixes a wide variety of other issues.</p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p><strong>Changes in version 0.2.3.9-alpha - 2011-12-08</strong><br />
<strong>Major features:</strong>
</p>

<ul>
<li>Clients can now connect to private bridges over IPv6. Bridges<br />
      still need at least one IPv4 address in order to connect to<br />
      other relays. Note that we don't yet handle the case where the<br />
      user has two bridge lines for the same bridge (one IPv4, one<br />
      IPv6). Implements parts of proposal 186.</li>
<li>New "DisableNetwork" config option to prevent Tor from launching any<br />
      connections or accepting any connections except on a control port.<br />
      Bundles and controllers can set this option before letting Tor talk<br />
      to the rest of the network, for example to prevent any connections<br />
      to a non-bridge address. Packages like Orbot can also use this<br />
      option to instruct Tor to save power when the network is off.</li>
<li>Clients and bridges can now be configured to use a separate<br />
      "transport" proxy. This approach makes the censorship arms race<br />
      easier by allowing bridges to use protocol obfuscation plugins. It<br />
      implements the "managed proxy" part of proposal 180 (ticket 3472).</li>
<li>When using OpenSSL 1.0.0 or later, use OpenSSL's counter mode<br />
      implementation. It makes AES_CTR about 7% faster than our old one<br />
      (which was about 10% faster than the one OpenSSL used to provide).<br />
      Resolves ticket 4526.</li>
<li>Add a "tor2web mode" for clients that want to connect to hidden<br />
      services non-anonymously (and possibly more quickly). As a safety<br />
      measure to try to keep users from turning this on without knowing<br />
      what they are doing, tor2web mode must be explicitly enabled at<br />
      compile time, and a copy of Tor compiled to run in tor2web mode<br />
      cannot be used as a normal Tor client. Implements feature 2553.</li>
<li>Add experimental support for running on Windows with IOCP and no<br />
      kernel-space socket buffers. This feature is controlled by a new<br />
      "UserspaceIOCPBuffers" config option (off by default), which has<br />
      no effect unless Tor has been built with support for bufferevents,<br />
      is running on Windows, and has enabled IOCP. This may, in the long<br />
      run, help solve or mitigate bug 98.</li>
<li>Use a more secure consensus parameter voting algorithm. Now at<br />
      least three directory authorities or a majority of them must<br />
      vote on a given parameter before it will be included in the<br />
      consensus. Implements proposal 178.</li>
</ul>

<p><strong>Major bugfixes:</strong>
</p>

<ul>
<li>Hidden services now ignore the timestamps on INTRODUCE2 cells.<br />
      They used to check that the timestamp was within 30 minutes<br />
      of their system clock, so they could cap the size of their<br />
      replay-detection cache, but that approach unnecessarily refused<br />
      service to clients with wrong clocks. Bugfix on 0.2.1.6-alpha, when<br />
      the v3 intro-point protocol (the first one which sent a timestamp<br />
      field in the INTRODUCE2 cell) was introduced; fixes bug 3460.</li>
<li>Only use the EVP interface when AES acceleration is enabled,<br />
      to avoid a 5-7% performance regression. Resolves issue 4525;<br />
      bugfix on 0.2.3.8-alpha.</li>
</ul>

<p><strong>Privacy/anonymity features (bridge detection):</strong>
</p>

<ul>
<li>Make bridge SSL certificates a bit more stealthy by using random<br />
      serial numbers, in the same fashion as OpenSSL when generating<br />
      self-signed certificates. Implements ticket 4584.</li>
<li>Introduce a new config option "DynamicDHGroups", enabled by<br />
      default, which provides each bridge with a unique prime DH modulus<br />
      to be used during SSL handshakes. This option attempts to help<br />
      against censors who might use the Apache DH modulus as a static<br />
      identifier for bridges. Addresses ticket 4548.</li>
</ul>

<p><strong>Minor features (new/different config options):</strong>
</p>

<ul>
<li>New configuration option "DisableDebuggerAttachment" (on by default)<br />
      to prevent basic debugging attachment attempts by other processes.<br />
      Supports Mac OS X and Gnu/Linux. Resolves ticket 3313.</li>
<li>Allow MapAddress directives to specify matches against super-domains,<br />
      as in "MapAddress *.torproject.org *.torproject.org.torserver.exit".<br />
      Implements issue 933.</li>
<li>Slightly change behavior of "list" options (that is, config<br />
      options that can appear more than once) when they appear both in<br />
      torrc and on the command line. Previously, the command-line options<br />
      would be appended to the ones from torrc. Now, the command-line<br />
      options override the torrc options entirely. This new behavior<br />
      allows the user to override list options (like exit policies and<br />
      ports to listen on) from the command line, rather than simply<br />
      appending to the list.</li>
<li>You can get the old (appending) command-line behavior for "list"<br />
      options by prefixing the option name with a "+".</li>
<li>You can remove all the values for a "list" option from the command<br />
      line without adding any new ones by prefixing the option name<br />
      with a "/".</li>
<li>Add experimental support for a "defaults" torrc file to be parsed<br />
      before the regular torrc. Torrc options override the defaults file's<br />
      options in the same way that the command line overrides the torrc.<br />
      The SAVECONF controller command saves only those options which<br />
      differ between the current configuration and the defaults file. HUP<br />
      reloads both files. (Note: This is an experimental feature; its<br />
      behavior will probably be refined in future 0.2.3.x-alpha versions<br />
      to better meet packagers' needs.)</li>
</ul>

<p><strong>Minor features:</strong>
</p>

<ul>
<li>Try to make the introductory warning message that Tor prints on<br />
      startup more useful for actually finding help and information.<br />
      Resolves ticket 2474.</li>
<li>Running "make version" now displays the version of Tor that<br />
      we're about to build. Idea from katmagic; resolves issue 4400.</li>
<li>Expire old or over-used hidden service introduction points.<br />
      Required by fix for bug 3460.</li>
<li>Move the replay-detection cache for the RSA-encrypted parts of<br />
      INTRODUCE2 cells to the introduction point data structures.<br />
      Previously, we would use one replay-detection cache per hidden<br />
      service. Required by fix for bug 3460.</li>
<li>Reduce the lifetime of elements of hidden services' Diffie-Hellman<br />
      public key replay-detection cache from 60 minutes to 5 minutes. This<br />
      replay-detection cache is now used only to detect multiple<br />
      INTRODUCE2 cells specifying the same rendezvous point, so we can<br />
      avoid launching multiple simultaneous attempts to connect to it.</li>
</ul>

<p><strong>Minor bugfixes (on Tor 0.2.2.x and earlier):</strong>
</p>

<ul>
<li>Resolve an integer overflow bug in smartlist_ensure_capacity().<br />
      Fixes bug 4230; bugfix on Tor 0.1.0.1-rc. Based on a patch by<br />
      Mansour Moufid.</li>
<li>Fix a minor formatting issue in one of tor-gencert's error messages.<br />
      Fixes bug 4574.</li>
<li>Prevent a false positive from the check-spaces script, by disabling<br />
      the "whitespace between function name and (" check for functions<br />
      named 'op()'.</li>
<li>Fix a log message suggesting that people contact a non-existent<br />
      email address. Fixes bug 3448.</li>
<li>Fix null-pointer access that could occur if TLS allocation failed.<br />
      Fixes bug 4531; bugfix on 0.2.0.20-rc. Found by "troll_un".</li>
<li>Report a real bootstrap problem to the controller on router<br />
      identity mismatch. Previously we just said "foo", which probably<br />
      made a lot of sense at the time. Fixes bug 4169; bugfix on<br />
      0.2.1.1-alpha.</li>
<li>If we had ever tried to call tor_addr_to_str() on an address of<br />
      unknown type, we would have done a strdup() on an uninitialized<br />
      buffer. Now we won't. Fixes bug 4529; bugfix on 0.2.1.3-alpha.<br />
      Reported by "troll_un".</li>
<li>Correctly detect and handle transient lookup failures from<br />
      tor_addr_lookup(). Fixes bug 4530; bugfix on 0.2.1.5-alpha.<br />
      Reported by "troll_un".</li>
<li>Use tor_socket_t type for listener argument to accept(). Fixes bug<br />
      4535; bugfix on 0.2.2.28-beta. Found by "troll_un".</li>
<li>Initialize conn-&gt;addr to a valid state in spawn_cpuworker(). Fixes<br />
      bug 4532; found by "troll_un".</li>
</ul>

<p><strong>Minor bugfixes (on Tor 0.2.3.x):</strong>
</p>

<ul>
<li>Fix a compile warning in tor_inet_pton(). Bugfix on 0.2.3.8-alpha;<br />
      fixes bug 4554.</li>
<li>Don't send two ESTABLISH_RENDEZVOUS cells when opening a new<br />
      circuit for use as a hidden service client's rendezvous point.<br />
      Fixes bugs 4641 and 4171; bugfix on 0.2.3.3-alpha. Diagnosed<br />
      with help from wanoskarnet.</li>
<li>Restore behavior of overriding SocksPort, ORPort, and similar<br />
      options from the command line. Bugfix on 0.2.3.3-alpha.</li>
</ul>

<p><strong>Build fixes:</strong>
</p>

<ul>
<li>Properly handle the case where the build-tree is not the same<br />
      as the source tree when generating src/common/common_sha1.i,<br />
      src/or/micro-revision.i, and src/or/or_sha1.i. Fixes bug 3953;<br />
      bugfix on 0.2.0.1-alpha.</li>
</ul>

<p><strong>Code simplifications, cleanups, and refactorings:</strong>
</p>

<ul>
<li>Remove the pure attribute from all functions that used it<br />
      previously. In many cases we assigned it incorrectly, because the<br />
      functions might assert or call impure functions, and we don't have<br />
      evidence that keeping the pure attribute is worthwhile. Implements<br />
      changes suggested in ticket 4421.</li>
<li>Remove some dead code spotted by coverity. Fixes cid 432.<br />
      Bugfix on 0.2.3.1-alpha, closes bug 4637.</li>
</ul>

---
_comments:

<a id="comment-13052"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13052" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 11, 2011</p>
    </div>
    <a href="#comment-13052">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13052" class="permalink" rel="bookmark">I&#039;ve downloaded the Vidalia</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've downloaded the Vidalia bundle for this new build (<a href="https://www.torproject.org/dist/vidalia-bundles/vidalia-bundle-0.2.3.9-alpha-0.2.15.exe" rel="nofollow">https://www.torproject.org/dist/vidalia-bundles/vidalia-bundle-0.2.3.9-…</a>) and installed it, but I don't see any new config options for DisableNetwork, either in the Vidalia interface, or by looking at the sample torrc file... So I looked at the Manual and at the Dev Manual (<a href="https://www.torproject.org/docs/tor-manual-dev.html.en" rel="nofollow">https://www.torproject.org/docs/tor-manual-dev.html.en</a>), and don't see the option there.  </p>
<p>How does one configure the DisableNetwork option??</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13059"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13059" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2011</p>
    </div>
    <a href="#comment-13059">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13059" class="permalink" rel="bookmark">Hi All,
When enabling</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi All,</p>
<p>When enabling accounting with the 0.2.3.9-alpha build, downloaded at the Tor web site, I get the following on starting it:</p>
<p>[Error] init_dh_param(): Bug: crypto.c:2093: init_dh_param: Assertion<br />
dh_param_p_tls failed; aborting.</p>
<p>This is with accounting parameters set to daily bandwidth allowance.</p>
<p>If I remove accounting, the assert() does not fire on initial startup, and all is well with Tor.</p>
<p>Has this been verified anywhere else ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13060"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13060" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2011</p>
    </div>
    <a href="#comment-13060">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13060" class="permalink" rel="bookmark">A ticket: Ticket #4702 has</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A ticket: Ticket #4702 has been raised for the Accounting bug against this release.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13061"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13061" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2011</p>
    </div>
    <a href="#comment-13061">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13061" class="permalink" rel="bookmark">&gt;Note that we don&#039;t yet</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt;Note that we don't yet handle the case where the<br />
user has two bridge lines for the same bridge (one IPv4, one<br />
IPv6)</p>
<p>I think should allow user choose ipv4 or ipv6.  now all ipv4 blocked in china. only use ipv6 for normal visit just now.</p>
<p>why new tor version not show relay list ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13064"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13064" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2011</p>
    </div>
    <a href="#comment-13064">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13064" class="permalink" rel="bookmark">never had an issue updating</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>never had an issue updating tor alpha versions on Ubuntu, but this version would not install because it required libevent-2.0-5_2.0.16-stable-1 but this was not going to be installed with 0.2.3.9 alpha. Long story short, installed the package separately then was able to update the alpha release.</p>
</div>
  </div>
</article>
<!-- Comment END -->
