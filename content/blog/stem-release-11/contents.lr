title: Stem Release 1.1
---
pub_date: 2013-10-14
---
author: atagar
---
tags: stem
---
_html_body:

<p>Hi all. After seven months of work I'm pleased to announce Stem's 1.1.0 release!</p>

<p>For those who aren't familiar with it, Stem is a Python library for interacting with Tor. With it you can script against your relay, descriptor data, or even write applications similar to <a href="https://www.atagar.com/arm/" rel="nofollow">arm</a> and <a href="https://www.torproject.org/projects/vidalia.html.en" rel="nofollow">Vidalia</a>.</p>

<p><b><a href="https://stem.torproject.org/" rel="nofollow">https://stem.torproject.org/</a></b></p>

<p>So what's new in this release?</p>

<h2>Remote Descriptor Fetching</h2>

<p>The <a href="https://stem.torproject.org/api/descriptor/remote.html" rel="nofollow">stem.descriptor.remote</a> module allows you download current Tor descriptor data from directory authorities and mirrors, much like Tor itself does. With this you can easily check the status of the network without piggybacking on a local instance of Tor (or even having it installed).</p>

<p>For example...</p>

<p>from stem.descriptor.remote import DescriptorDownloader</p>

<p>downloader = DescriptorDownloader()</p>

<p>try:<br />
  for desc in downloader.get_consensus().run():<br />
    print "found relay %s (%s)" % (desc.nickname, desc.fingerprint)<br />
except Exception as exc:<br />
  print "Unable to retrieve the consensus: %s" % exc</p>

<h2>Connection Resolution</h2>

<p>One of arm's most popular features has been its ability to monitor Tor's connections, and stem can now do the same! Lookups are performed via seven *nix and FreeBSD resolvers, for more information and an example see <a href="https://stem.torproject.org/tutorials/east_of_the_sun.html#connection-resolution" rel="nofollow">our tutorials</a>.</p>

<h2>Numerous Features and Fixes</h2>

<p>For a rundown of other changes see...</p>

<p><b><a href="https://stem.torproject.org/change_log.html#version-1-1" rel="nofollow">https://stem.torproject.org/change_log.html#version-1-1</a></b></p>

<p>Cheers! -Damian</p>

---
_comments:

<a id="comment-36232"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36232" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 14, 2013</p>
    </div>
    <a href="#comment-36232">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36232" class="permalink" rel="bookmark">https://check2.torproject.org</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://check2.torproject.org/" rel="nofollow">https://check2.torproject.org/</a> is great, if only you display the exit node's info on the same page instead of displaying a link to it, otherwise it isn't really different than <a href="https://check.torproject.org" rel="nofollow">https://check.torproject.org</a> !</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-36279"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36279" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 16, 2013</p>
    </div>
    <a href="#comment-36279">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36279" class="permalink" rel="bookmark">today, in China, previous</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>today, in China, previous obfs bridges out of use</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-36560"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36560" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2013</p>
    </div>
    <a href="#comment-36560">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36560" class="permalink" rel="bookmark">Hello,
Stem is an awesome</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,<br />
Stem is an awesome library, however, I don't know if it's possible to control remote relays, in similar way when we use the Controller object to manage local instances... there's any way to do that?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-38627"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38627" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 23, 2013</p>
    </div>
    <a href="#comment-38627">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38627" class="permalink" rel="bookmark">This all seems great stuff</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This all seems great stuff you guys are putting out but I have to ask do we all have to be at the top of the Computer tree, as I am a sliver surfer and most of the things you write go straight over my head..</p>
</div>
  </div>
</article>
<!-- Comment END -->
