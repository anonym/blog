title: Tor’s Bug Smash Fund, Year 2: $106,709 Raised!
---
pub_date: 2020-09-17
---
author: alsmith
---
tags: fundraising
---
categories: fundraising
---
summary:

This August, we asked you to help us fundraise for our second annual Bug Smash Fund campaign. In 2020, despite the challenges of COVID-19 and event cancellations, you helped us to raise $106,709! 
---
_html_body:

<p style="color: red; font-size: 22px;"><strong>*Note: The Tor Project's postal address has changed since this post was published. Find the most current address in <a href="https://donate.torproject.org/donor-faq/#can-i-donate-with-a-check-cash-or-money-order">our FAQ</a>.</strong></p>

<p>Let’s start this post with a rousing THANK YOU to the Tor community!</p>
<p>This August, <a href="https://blog.torproject.org/tor-bug-smash-fund-2020">we asked you to help us fundraise for our second annual Bug Smash Fund campaign</a>. This fund is designed to grow a healthy reserve earmarked for maintenance work, finding bugs, and smashing them—all tasks necessary to keep Tor Browser, the Tor network, and the many tools that rely on Tor strong, safe, and running smoothly.</p>
<p>In 2019, we raised $86,081, half of which we raised in-person at DEFCON.</p>
<p><strong>In 2020, despite the challenges of COVID-19 and event cancellations, you helped us to raise $106,709! </strong></p>
<p>We could not have predicted how successful this campaign would be. We’re living in a world with new limitations and challenges, and we know many of you are impacted by the financial impact of the pandemic and its ripple effects, as are we. Despite that, contributions came from all over the world, in all different forms: online, through the mail, and in a multitude of different cryptocurrencies, with individual and organizational campaigns supporting the Bug Smash Fund in a variety of efforts. We are humbled by your generosity and continued encouragement around the importance of this fund. Thank you!</p>
<p>Additionally, 60% of all donations came in the form of cryptocurrency. Thank you to everyone from the cryptocurrency community for supporting Tor—you’ve made a huge impact on our ability to find and fix bugs.</p>
<p>What’s next: as we did in 2019, we’ll tag bugs in GitLab with <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues?label_name%5B%5D=BugSmashFund&amp;scope=all&amp;state=all">BugSmashFund</a> so you can follow along with how we’re allocating the fund. We’ll also make periodic updates here on the blog and through the newsletter about our progress with these BugSmashFund tickets. </p>
<p>Thank you to everybody who made a contribution to the Bug Smash Fund. This work is critical in helping us to provide safer tools for millions of people around the world exercising their human rights to privacy and freedom online.</p>
<p>P.S.: If you made a gift of $74 in order to receive a surprise t-shirt pack during this campaign, your gift is in the mail and on its way to you now.</p>

---
_comments:

<a id="comment-289483"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289483" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 18, 2020</p>
    </div>
    <a href="#comment-289483">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289483" class="permalink" rel="bookmark">Good work, keep it up!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good work, keep it up!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289496"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289496" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Michelle (not verified)</span> said:</p>
      <p class="date-time">September 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289483" class="permalink" rel="bookmark">Good work, keep it up!</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289496">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289496" class="permalink" rel="bookmark">Proud of you.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Proud of you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289510"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289510" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 21, 2020</p>
    </div>
    <a href="#comment-289510">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289510" class="permalink" rel="bookmark">@ Tor Project mail clerk:
&gt;…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ Tor Project mail clerk:</p>
<p>&gt; $106,709 Raised</p>
<p>The true figure is larger.  Can you guess what the problem is?</p>
<p>Be aware that in recent weeks about half the US Mail I have sent to</p>
<p>The Tor Project<br />
217 1st Ave South #4903<br />
Seattle, WA 98194 USA</p>
<p>has been returned marked with one of those horrid yellow stickers reading</p>
<p>RETURN TO SENDER<br />
UNABLE TO FORWARD<br />
UNABLE TO FORWARD<br />
RETURN TO SENDER</p>
<p>Since I sometimes receive mail from TP sent from the above address, and since your website gives this as the correct address, I believe it probably is the correct address, but it would not hurt to confirm that here.  TIA.</p>
<p>Three possibilities occur:</p>
<p>o general chaos in USPS resulting from DeJoy's fanatical attempts to destroy the US Mail (my mail carrier told me this morning he is now training the fifth new employee this year; the previous four all quit after a few weeks because DeJoy has made the job of mail carrier essentially impossible to perform and supervisors continually scream at honest mail carriers who are trying to cope with a service being systematically destroyed by its own chief),</p>
<p>o Drump's vow to deny federal funding/services to the city of Seattle on the basis of it's alleged status as an "anarchist jurisdiction"; see</p>
<p><a href="https://www.justice.gov/opa/pr/department-justice-identifies-new-york-city-portland-and-seattle-jurisdictions-permitting" rel="nofollow">https://www.justice.gov/opa/pr/department-justice-identifies-new-york-c…</a></p>
<p>o the latest in a long long line of secret (and largely illegal) FBI "disrupt/discredit/divide" schemes, specifically targeting a particularly hated entity (Tor Project) and its supporters.</p>
<p>I encourage TP to contact the district offices of your Congressional representatives.  Note that this is a problem with a federal agency, the kind of complaint they handle, and I have some reason to think that they are currently taking problems with USPS seriously.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289548"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289548" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">September 22, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289510" class="permalink" rel="bookmark">@ Tor Project mail clerk:
&gt;…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289548">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289548" class="permalink" rel="bookmark">I&#039;m not sure why that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm not sure why that happened because that is the correct address. How many letters have you sent that have been returned? You can email donations at tor project dot org if you feel better about corresponding that way. I'd like to look into it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289585"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289585" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 23, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289510" class="permalink" rel="bookmark">@ Tor Project mail clerk:
&gt;…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289585">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289585" class="permalink" rel="bookmark">Hmm. If a sender happened…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hmm. If a sender happened not to write a return address because they didn't want to be identified, then it couldn't be returned, and the letter might be thrown in the trash and/or opened and archived.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289656"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289656" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>hassnae (not verified)</span> said:</p>
      <p class="date-time">September 24, 2020</p>
    </div>
    <a href="#comment-289656">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289656" class="permalink" rel="bookmark">I understand thank you for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I understand thank you for all</p>
</div>
  </div>
</article>
<!-- Comment END -->
