title: Down to 0 issues on Coverity Scan.
---
pub_date: 2009-01-11
---
author: nickm
---
_html_body:

<p>As of 7 January, we're down to 0 issues on Coverity Scan.  This is great news!</p>

<p>In case you haven't heard of them, Coverity makes top-of-the-line static analysis tools (programs that analyse other programs looking for possible bugs).  They're a big serious company, with a serious "enterprise" pricing structure.  But, fortunately to us, they have <a href="http://scan.coverity.com/" rel="nofollow">a program</a> to provide the use of these tools, free of charge, to selected open source projects.  They've been scanning development snapshots of Tor for bugs since last September.</p>

<p>In September, they found 171 issues in our code.  Many of these were just sloppiness in our unit tests' error handing, but a good fraction were real bugs in our main codebase, a couple of which could have resulted in crashes under unusual circumstances that probably would have been hard to debug.  By December, we were down to 15 issues.  Now we're at 0, at long last.</p>

<p>Congratulations and thanks to everybody who helped analyse and fix the bugs here, and many thanks to the administrators of Coverity Scan for helping us out.</p>

---
_comments:

<a id="comment-505"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-505" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 11, 2009</p>
    </div>
    <a href="#comment-505">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-505" class="permalink" rel="bookmark">Blind?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>AM I blind? I can't find Tor on that website.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-509"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-509" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">January 11, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-505" class="permalink" rel="bookmark">Blind?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-509">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-509" class="permalink" rel="bookmark">I asked them the same</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I asked them the same question back in December.  It turns out they hadn't updated the official list (or most of the website) in a while.  I wish they would, but I am glad that they are better at adding projects than updating their website.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-510"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-510" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 12, 2009</p>
    </div>
    <a href="#comment-510">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-510" class="permalink" rel="bookmark">I&#039;m confused about</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm confused about something: The Tor network reveals the IP addresses of all its relay nodes (look at the "View the Network" button in your Vidalia control panel). Doesn't this reveal all the nodes running Tor, and therefore any Tor client and their IP address? It would seem to me that any person or organization with the ability to track down each IP address in the Tor network would still be able to find the user that they are looking for. Am misunderstanding something?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-511"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-511" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">January 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-510" class="permalink" rel="bookmark">I&#039;m confused about</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-511">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-511" class="permalink" rel="bookmark">Clients aren&#039;t nodes.  Nodes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Clients aren't nodes.  Nodes are volunteer-operated relays.  Clients do not relay traffic by default.</p>
<p>There are about 1200 nodes, give or take.  That's the list you see in Vidalia.[*]  There are hundreds of thousands of clients.  That list, you don't see.</p>
<p>[*] There are also nodes you don't see.  Read up on "bridges" to learn more about these.  The idea is to give people the option to run nodes that aren't advertised publicly in order to help people in censored countries.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-517"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-517" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 13, 2009</p>
    </div>
    <a href="#comment-517">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-517" class="permalink" rel="bookmark">Looks like there are more coverity bugs to fix next!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://scan.coverity.com/" rel="nofollow">http://scan.coverity.com/</a> points to "rung 0", "rung 1", "rung 2". We have made it<br />
to the end of rung 0. Now they've upgraded us to their newer scanning algorithm.</p>
<p>So, I suppose that means you should expect two more of these blogs posts,<br />
sometime in the future! :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-529"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-529" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 14, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-529">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-529" class="permalink" rel="bookmark">hmm</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wasn't aware Coverity was an mmorpg. ;)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-533"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-533" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>AnonymousCoward (not verified)</span> said:</p>
      <p class="date-time">January 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-533">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-533" class="permalink" rel="bookmark">lol ;)
gz anyway, well done,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>lol ;)<br />
gz anyway, well done, keep up the good work !</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-603"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-603" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 04, 2009</p>
    </div>
    <a href="#comment-603">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-603" class="permalink" rel="bookmark">Changes in version 0.2.0.33</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Changes in version 0.2.0.33 - 2009-01-21<br />
  o Security fixes:<br />
    - Fix a heap-corruption bug that may be remotely triggerable on<br />
      some platforms. Reported by Ilja van Sprundel.</p>
</div>
  </div>
</article>
<!-- Comment END -->
