title: New release: Tor 0.4.5.8
---
pub_date: 2021-05-10
---
author: nickm
---
tags: stable release
---
categories: releases
---
_html_body:

<p>We have a new stable release today. If you build Tor from source, you can download the source code for Tor 0.4.5.8 on the <a href="https://www.torproject.org/download/tor/">download page</a>. Packages should be available within the next several weeks, with a new Tor Browser likely next week.</p>
<p>Tor 0.4.5.8 fixes several bugs in earlier version, backporting fixes from the 0.4.6.x series.</p>
<h2>Changes in version 0.4.5.8 - 2021-05-10</h2>
<ul>
<li>Minor features (compatibility, Linux seccomp sandbox, backport from 0.4.6.3-rc):
<ul>
<li>Add a workaround to enable the Linux sandbox to work correctly with Glibc 2.33. This version of Glibc has started using the fstatat() system call, which previously our sandbox did not allow. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40382">40382</a>; see the ticket for a discussion of trade-offs.</li>
</ul>
</li>
<li>Minor features (compilation, backport from 0.4.6.3-rc):
<ul>
<li>Make the autoconf script build correctly with autoconf versions 2.70 and later. Closes part of ticket <a href="https://bugs.torproject.org/tpo/core/tor/40335">40335</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (fallback directory list, backport from 0.4.6.2-alpha):
<ul>
<li>Regenerate the list of fallback directories to contain a new set of 200 relays. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40265">40265</a>.</li>
</ul>
</li>
<li>Minor features (geoip data):
<ul>
<li>Update the geoip files to match the IPFire Location Database, as retrieved on 2021/05/07.</li>
</ul>
</li>
<li>Minor features (onion services):
<ul>
<li>Add warning message when connecting to now deprecated v2 onion services. As announced, Tor 0.4.5.x is the last series that will support v2 onions. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40373">40373</a>.</li>
</ul>
</li>
<li>Minor bugfixes (bridge, pluggable transport, backport from 0.4.6.2-alpha):
<ul>
<li>Fix a regression that made it impossible start Tor using a bridge line with a transport name and no fingerprint. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40360">40360</a>; bugfix on 0.4.5.4-rc.</li>
</ul>
</li>
<li>Minor bugfixes (build, cross-compilation, backport from 0.4.6.3-rc):
<ul>
<li>Allow a custom "ar" for cross-compilation. Our previous build script had used the $AR environment variable in most places, but it missed one. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40369">40369</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (channel, DoS, backport from 0.4.6.2-alpha):
<ul>
<li>Fix a non-fatal BUG() message due to a too-early free of a string, when listing a client connection from the DoS defenses subsystem. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40345">40345</a>; bugfix on 0.4.3.4-rc.</li>
</ul>
</li>
<li>Minor bugfixes (compiler warnings, backport from 0.4.6.3-rc):
<ul>
<li>Fix an indentation problem that led to a warning from GCC 11.1.1. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40380">40380</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (controller, backport from 0.4.6.1-alpha):
<ul>
<li>Fix a "BUG" warning that would appear when a controller chooses the first hop for a circuit, and that circuit completes. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40285">40285</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service, client, memory leak, backport from 0.4.6.3-rc):
<ul>
<li>Fix a bug where an expired cached descriptor could get overwritten with a new one without freeing it, leading to a memory leak. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40356">40356</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing, BSD, backport from 0.4.6.2-alpha):
<ul>
<li>Fix pattern-matching errors when patterns expand to invalid paths on BSD systems. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40318">40318</a>; bugfix on 0.4.5.1-alpha. Patch by Daniel Pinto.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291787"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291787" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 11, 2021</p>
    </div>
    <a href="#comment-291787">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291787" class="permalink" rel="bookmark">Thank you tor people :)
&gt;…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you tor people :)</p>
<p>&gt; Packages should be available within the next several weeks, with a new Tor Browser likely next week.</p>
<p>Hey I have a question I just thought of. I use qubes-whonix where the tor client runs in a separate VM and the browser's bundled Tor client is disabled. However the browser's built-in auto-update mechanism is not disabled in whonix.</p>
<p>Does this create a fingerprinting risk for whonix users during the time between when the browser updates itself, and when the Tor client package ships? I.e. due to using mismatched versions of Tor Browser and Tor client, compared to regular users who are using the bundled tor client?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291919"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291919" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 07, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291787" class="permalink" rel="bookmark">Thank you tor people :)
&gt;…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291919">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291919" class="permalink" rel="bookmark">I&#039;m not a developer, but I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm not a developer, but I don't think a mismatch of daemon versions would create a fingerprinting risk for TBB. If I understand correctly, your tor daemon is visible to only your first relay (guard or bridge), but the traffic and metadata of your browser (or any torified application that uses the daemon as a proxy) are encrypted from your daemon to the end of the circuit of relays. The exit relay doesn't receive data about your tor daemon as far as I know, but there's always the possibility of unstudied forms of traffic analysis and timing analysis.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291920"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291920" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 07, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291787" class="permalink" rel="bookmark">Thank you tor people :)
&gt;…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291920">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291920" class="permalink" rel="bookmark">Make sure the tor daemon…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Make sure the tor daemon that isn't bundled has its torrc file configured similarly to the one for the bundled daemon, e.g. not selecting or excluding specific sets of nodes or configuring other <a href="https://2019.www.torproject.org/docs/tor-manual.html.en" rel="nofollow">options</a> that could increase your fingerprint.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291804"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291804" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>question (not verified)</span> said:</p>
      <p class="date-time">May 14, 2021</p>
    </div>
    <a href="#comment-291804">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291804" class="permalink" rel="bookmark">Country in geoip file is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Country in geoip file is incorrect?<br />
geoip:sabotage=Seychelles, real=Netherlands.</p>
</div>
  </div>
</article>
<!-- Comment END -->
