title: Tor Weekly News — November 5th, 2014
---
pub_date: 2014-11-05
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the forty-fourth issue in 2014 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor 0.2.6.1-alpha is out</h1>

<p>Following last week’s stabilization of Tor 0.2.5.x, Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-talk/2014-October/035390.html" rel="nofollow">announced</a> the first alpha release in the Tor 0.2.6.x series. Quoting the changelog, this version “includes numerous code cleanups and new tests, and fixes a large number of annoying bugs. Out-of-memory conditions are handled better than in 0.2.5, pluggable transports have improved proxy support, and clients now use optimistic data for contacting hidden services.” Support for some very old compilers that do not understand the C99 programming standard, systems without threading support, and the Windows CE operating system has also been dropped.</p>

<p>“This is the first alpha release in a new series, so expect there to be bugs.” If you want to test it out, you can find the source code in the <a href="https://dist.torproject.org/" rel="nofollow">distribution directory</a>.</p>

<h1>Tor Browser 4.0.1 is out</h1>

<p>Mike Perry <a href="https://blog.torproject.org/blog/tor-browser-401-released" rel="nofollow">announced</a> a bugfix release by the Tor Browser team.  This version disables <a href="https://en.wikipedia.org/wiki/DirectShow" rel="nofollow">DirectShow</a>, which was causing the Windows build of Tor Browser to <a href="https://bugs.torproject.org/13443" rel="nofollow">crash</a> when visiting many websites. This is not a security release, but Windows users who have experienced this issue should upgrade.</p>

<p>Please see Mike’s post for the changelog, and download your copy from the <a href="https://www.torproject.org/projects/torbrowser.html" rel="nofollow">project page</a>.</p>

<h1>Facebook, hidden services, and HTTPS certificates</h1>

<p>Facebook, one of the world’s most popular websites, <a href="https://www.facebook.com/notes/protect-the-graph/making-connections-to-facebook-more-secure/1526085754298237" rel="nofollow">surprised the Internet</a> by becoming the most prominent group so far to set up a Tor hidden service. Rather than connecting through an exit relay, Facebook users can now interact with the social network without their traffic leaving the Tor network at all until it reaches its destination.</p>

<p>Soon after the service was announced, some in the Tor community expressed concern over the implications of its <a href="https://lists.torproject.org/pipermail/tor-talk/2014-October/035403.html" rel="nofollow">unusually memorable .onion address</a>. Had Facebook somehow mustered the computing power to brute-force hidden service keys at will? Alec Muffett, one of the lead engineers behind the project, <a href="https://lists.torproject.org/pipermail/tor-talk/2014-October/035413.html" rel="nofollow">clarified</a> that in fact “we just did the same thing as everyone else: generated a bunch of keys with a fixed lead prefix (‘facebook’) and then went fishing looking for good ones”, getting “tremendous lucky” in the process. Those concerned by how easy this seems, <a href="https://lists.torproject.org/pipermail/tor-talk/2014-October/035416.html" rel="nofollow">added</a> Nick Mathewson, “might want to jump in on reviewing and improving <a href="https://gitweb.torproject.org/torspec.git/blob_plain/HEAD:/proposals/224-rend-spec-ng.txt" rel="nofollow">proposal 224</a>, which includes a brand-new, even less usable, but far more secure, name format”.</p>

<p>“Why would you want to use Facebook over Tor?” remains a frequently-asked (and -misunderstood) question, so Roger Dingledine took to the <a href="https://blog.torproject.org/blog/facebook-hidden-services-and-https-certs" rel="nofollow">Tor blog</a> to address this and related issues. “The key point here is that anonymity isn’t just about hiding from your destination. There’s no reason to let your ISP know when or whether you’re visiting Facebook. There’s no reason for Facebook’s upstream ISP, or some agency that surveils the Internet, to learn when and whether you use Facebook. And if you do choose to tell Facebook something about you, there’s still no reason to let them automatically discover what city you’re in today while you do it.” Not only that, but Facebook is now taking advantage of the special security properties that hidden services provide, including strong authentication (letting users be confident that they are talking to the right server, and not to an impostor) and end-to-end encryption of their data.</p>

<p>This last point generated some confusion, since Facebook have also acquired an HTTPS certificate for their hidden service, which might seem like an unnecessary belt-and-suspenders approach to security. This has been the subject of “feisty discussions” in the Internet security community, with many points for and against: on the one hand, users have been taught that “https is necessary and http is scary, so it makes sense that users want to see the string “https” in front of” URLs, while on the other, “by encouraging people to pay Digicert we’re reinforcing the certificate authority business model when maybe we should be continuing to demonstrate an alternative.”</p>

<p>Please see Roger’s post for a fuller discussion of all these points and more, and feel free to contribute your own thoughts on the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-talk" rel="nofollow">tor-talk mailing list</a>. If you experience problems with the service, please contact Facebook support rather than the Tor help desk; as Alec wrote in the announcement, “we expect the service to be of an evolutionary and slightly flaky nature”, as it is an “experiment” — hopefully an experiment that will, as Roger suggested, “help to continue opening people’s minds about why they might want to offer a hidden service, and help other people think of further novel uses for hidden services.”</p>

<h1>Monthly status reports for October 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of October has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2014-October/000677.html" rel="nofollow">Juha Nurmi</a> released his report first, followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-October/000678.html" rel="nofollow">Georg Koppen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-October/000679.html" rel="nofollow">Sherief Alaa</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-October/000680.html" rel="nofollow">Pearl Crescent</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000682.html" rel="nofollow">Lunar</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000683.html" rel="nofollow">Harmony</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000684.html" rel="nofollow">Sukhbir Singh</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000685.html" rel="nofollow">Colin C.</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000687.html" rel="nofollow">Leiah Jansen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000688.html" rel="nofollow">Nick Mathewson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000689.html" rel="nofollow">Arlo Breault</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000690.html" rel="nofollow">Noel Torres</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000691.html" rel="nofollow">George Kadianakis</a>.</p>

<p>Lunar reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000681.html" rel="nofollow">help desk</a>, Arturo Filastò for the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000686.html" rel="nofollow">OONI team</a>, and Mike Perry for the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000692.html" rel="nofollow">Tor Browser team</a>.</p>

<h1>Miscellaneous news</h1>

<p>Mike Perry <a href="https://lists.torproject.org/pipermail/tbb-dev/2014-October/000148.html" rel="nofollow">updated</a> the <a href="https://www.torproject.org/projects/torbrowser/design/" rel="nofollow">Tor Browser design document</a> to cover Tor Browser version 4.0 — “Feedback welcome! Patches are even more welcomer!”</p>

<p>Israel Leiva sent out an <a href="https://lists.torproject.org/pipermail/tor-dev/2014-October/007700.html" rel="nofollow">update</a> on the progress of the GetTor redevelopment project.</p>

<p>David Fifield <a href="https://lists.torproject.org/pipermail/tor-dev/2014-October/007697.html" rel="nofollow">distributed</a> a <a href="https://people.torproject.org/~dcf/graphs/relays-all.pdf" rel="nofollow">graph</a> of “the number of simultaneous relay users for every country, one country per row”.</p>

<p>David also sent out a <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007716.html" rel="nofollow">summary</a> of the costs incurred by the meek pluggable transport, which have increased significantly following its incorporation into the latest stable Tor Browser and the consequent “explosion” in use.</p>

<p>Esfandiar Mohammadi <a href="https://lists.torproject.org/pipermail/tor-dev/2014-October/007692.html" rel="nofollow">announced</a> the <a href="http://www.infsec.cs.uni-saarland.de/projects/anonymity-guarantees/mator.html" rel="nofollow">MATor</a> project and accompanying paper. MATor is a tool that “assesses the influence of Tor’s path selection on a user’s anonymity”; “since MATor is an ongoing project, we would appreciate your opinion about the approach in general.”</p>

<h1>Tor help desk roundup</h1>

<p>The help desk has been asked if Tor Browser acts as a relay by default. Tor Browser’s Tor by default acts only as a client, and not as a bridge relay, exit relay, or relay. Additionally, this is <a href="https://www.torproject.org/docs/faq#EverybodyARelay" rel="nofollow">unlikely to change</a> in the future.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, Matt Pagan, Karsten Loesing, and Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

