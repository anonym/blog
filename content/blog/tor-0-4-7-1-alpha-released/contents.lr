title: New Alpha Release: Tor 0.4.7.1-alpha
---
pub_date: 2021-09-17
---
author: ahf
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.7.1-alpha from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely some time next week.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>This version is the first alpha release of the 0.4.7.x series. One major feature is Vanguards Lite, from proposal 333, to help mitigate guard discovery attacks against onion services. It also includes numerous bugfixes.</p>
<h2>Changes in version 0.4.7.1-alpha - 2021-09-17</h2>
<ul>
<li>Major features (Proposal 332, onion services, guard selection algorithm):
<ul>
<li>Clients and onion services now choose four long-lived "layer 2" guard relays for use as the middle hop in all onion circuits. These relays are kept in place for a randomized duration averaging 1 week. This mitigates guard discovery attacks against clients and short-lived onion services such as OnionShare. Long-lived onion services that need high security should still use the Vanguards addon (<a href="https://github.com/mikeperry-tor/vanguards">https://github.com/mikeperry-tor/vanguards</a>). Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40363">40363</a>; implements proposal 333.</li>
</ul>
</li>
<li>Minor features (bridge testing support):
<ul>
<li>Let external bridge reachability testing tools discard cached bridge descriptors when setting new bridges, so they can be sure to get a clean reachability test. Implements ticket <a href="https://bugs.torproject.org/tpo/core/tor/40209">40209</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (fuzzing):
<ul>
<li>When building with --enable-libfuzzer, use a set of compiler flags that works with more recent versions of the library. Previously we were using a set of flags from 2017. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40407">40407</a>.</li>
</ul>
</li>
<li>Minor features (testing configuration):
<ul>
<li>When TestingTorNetwork is enabled, skip the permissions check on hidden service directories. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40338">40338</a>.</li>
<li>On a testing network, relays can now use the TestingMinTimeToReportBandwidth option to change the smallest amount of time over which they're willing to report their observed maximum bandwidth. Previously, this was fixed at 1 day. For safety, values under 2 hours are only supported on testing networks. Part of a fix for ticket <a href="https://bugs.torproject.org/tpo/core/tor/40337">40337</a>.</li>
<li>Relays on testing networks no longer rate-limit how frequently they are willing to report new bandwidth measurements. Part of a fix for ticket <a href="https://bugs.torproject.org/tpo/core/tor/40337">40337</a>.</li>
<li>Relays on testing networks now report their observed bandwidths immediately from startup. Previously, they waited until they had been running for a full day. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40337">40337</a>.</li>
</ul>
</li>
<li>Minor bugfixes (circuit padding):
<ul>
<li>Don't send STOP circuit padding cells when the other side has already shut down the corresponding padding machine. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40435">40435</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compatibility):
<ul>
<li>Fix compatibility with the most recent Libevent versions, which no longer have an evdns_set_random_bytes() function. Because this function has been a no-op since Libevent 2.0.4-alpha, it is safe for us to just stop calling it. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40371">40371</a>; bugfix on 0.2.1.7-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (control, sandbox):
<ul>
<li>Allows the control command SAVECONF to succeed when the seccomp sandbox is enabled. Makes SAVECONF keep only one backup file, to simplify implementation. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40317">40317</a>; bugfix on 0.2.5.4-alpha. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor bugfixes (heartbeat):
<ul>
<li>Adjust the heartbeat log message about distinct clients to consider the HeartbeatPeriod rather than a flat 6-hour delay. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40330">40330</a>; bugfix on 0.2.6.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging, relay):
<ul>
<li>Add spaces between the "and" when logging the "Your server has not managed to confirm reachability for its" on dual-stack relays. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40453">40453</a>; bugfix on 0.4.5.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (onion service):
<ul>
<li>Do not flag an HSDir as non-running in case the descriptor upload or fetch fails. An onion service closes pending directory connections before uploading a new descriptor which leads to wrongly flagging many relays and thus affecting circuit path selection. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40434">40434</a>; bugfix on 0.2.0.13-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (statistics):
<ul>
<li>Fix a fencepost issue when we check stability_last_downrated where we called rep_hist_downrate_old_runs() twice. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40394">40394</a>; bugfix on 0.2.0.5-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (tests):
<ul>
<li>Fix a bug that prevented some tests from running with the correct names. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40365">40365</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Add links to original tor design paper and anonbib to docs/HACKING/README.1st.md. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/33742">33742</a>. Patch from Emily Bones.</li>
<li>Describe the "fingerprint-ed25519" file in the tor.1 man page. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40467">40467</a>; bugfix on 0.4.3.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292803"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292803" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 20, 2021</p>
    </div>
    <a href="#comment-292803">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292803" class="permalink" rel="bookmark">For readers, more…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For readers, more information about the Vanguards add-on is on the Operational Security (OpSec) page of the onion service advanced settings guide:<br />
<a href="https://community.torproject.org/onion-services/advanced/opsec/" rel="nofollow">https://community.torproject.org/onion-services/advanced/opsec/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292805"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292805" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Quanto (not verified)</span> said:</p>
      <p class="date-time">September 20, 2021</p>
    </div>
    <a href="#comment-292805">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292805" class="permalink" rel="bookmark">Please look into this tested…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please look into this tested and proven method of quantum proofing Tor. Quantum computers are less than 10 years away. All data passing now can be decrypted at some point in the near future.</p>
<p><a href="https://essay.utwente.nl/79710/" rel="nofollow">https://essay.utwente.nl/79710/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292828"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292828" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 23, 2021</p>
    </div>
    <a href="#comment-292828">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292828" class="permalink" rel="bookmark">Clients and onion services…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>Clients and onion services now choose four long-lived "layer 2" guard relays for use as the middle hop in all onion circuits.</p></blockquote>
<p>Why not extend this to all circuits (not just onion circuits)?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292831"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292831" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>@pples (not verified)</span> said:</p>
      <p class="date-time">September 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292828" class="permalink" rel="bookmark">Clients and onion services…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292831">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292831" class="permalink" rel="bookmark">So how does that prevent…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So how does that prevent decryption?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292844"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292844" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 26, 2021</p>
    </div>
    <a href="#comment-292844">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292844" class="permalink" rel="bookmark">I got an IPv6-only relay…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I got an IPv6-only relay configured and sitting here waiting to start accepting traffic one of these days...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292853"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292853" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 27, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292844" class="permalink" rel="bookmark">I got an IPv6-only relay…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292853">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292853" class="permalink" rel="bookmark">&gt; IPv6-only
Tor relays…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; IPv6-only</p>
<p>Tor relays require IPv4 at minimum for the time being.</p>
<p><a href="https://community.torproject.org/relay/relays-requirements/" rel="nofollow">https://community.torproject.org/relay/relays-requirements/</a><br />
<a href="https://community.torproject.org/relay/setup/post-install/" rel="nofollow">https://community.torproject.org/relay/setup/post-install/</a><br />
<a href="https://community.torproject.org/relay/getting-help/" rel="nofollow">https://community.torproject.org/relay/getting-help/</a><br />
<a href="https://support.torproject.org/relay-operators/ipv6-relay/" rel="nofollow">https://support.torproject.org/relay-operators/ipv6-relay/</a><br />
<a href="https://support.torproject.org/relay-operators/why-isnt-my-relay-being-used-more/" rel="nofollow">https://support.torproject.org/relay-operators/why-isnt-my-relay-being-…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292851"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292851" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 27, 2021</p>
    </div>
    <a href="#comment-292851">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292851" class="permalink" rel="bookmark">Can you link to proposal 333…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you link to proposal 333 please?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292928"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292928" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 06, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292851" class="permalink" rel="bookmark">Can you link to proposal 333…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292928">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292928" class="permalink" rel="bookmark">Here are all the proposals:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Here are all the proposals: <a href="https://gitweb.torproject.org/torspec.git/tree/proposals/" rel="nofollow">https://gitweb.torproject.org/torspec.git/tree/proposals/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292927"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292927" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 06, 2021</p>
    </div>
    <a href="#comment-292927">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292927" class="permalink" rel="bookmark">In ticket 40363 for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In ticket 40363 for vanguards lite, <a href="https://gitlab.torproject.org/tpo/core/tor/-/issues/40363#note_2733163" rel="nofollow">Mike Perry wrote</a>, "It's not impossible that a user might have a malicious tab open that entire time. If we look at <a href="https://gitweb.torproject.org/torspec.git/tree/proposals/247-hs-guard-discovery.txt#n300" rel="nofollow">https://gitweb.torproject.org/torspec.git/tree/proposals/247-hs-guard-d…</a>, then 30 rotations is still 60% success even for a 1% adversary, with a 24 hour rotation period, and 99% success for a 5% adversary... We should raise this to a longer average. 1 week would make it 10% success probability for 1% adversary, and 50% for 5%. And/or we could use 3 L2 guards."</p>
<p>If the probability exceeds a threshold, should Tor Browser warn the user and tell them to close the tab or start a new identity? Although, this is for little-t tor, so it isn't exclusive to browser tabs. It couldn't communicate a warning to applications about anything.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-293025"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293025" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 18, 2021</p>
    </div>
    <a href="#comment-293025">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293025" class="permalink" rel="bookmark">&gt;Major features (Proposal…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt;Major features (<b>Proposal 332</b>, onion services, guard selection algorithm):<br />
&gt;[....]<br />
&gt;Closes ticket 40363; implements <b>proposal 333</b>.<br />
Looks like a typo.</p>
</div>
  </div>
</article>
<!-- Comment END -->
