title: Help Tor Smash Bugs: August 1-31!
---
pub_date: 2021-07-27
---
author: alsmith
---
summary: The Bug Smash Fund is back for its third year! In 2019, we launched Tor’s Bug Smash Fund to raise money that would support our developers finding and fixing bugs in our software and to conduct routine maintenance. Maintenance isn’t a flashy new feature, and that makes it less interesting to many traditional funders, but it’s what keeps the reliable stuff working--and with your support, we’ve closed 370 Bug Smash Fund tickets.
---
_html_body:

<p>The Bug Smash Fund is back for its third year! In 2019, <a href="https://blog.torproject.org/tors-bug-smash-fund-help-tor-smash-all-bugs">we launched Tor’s Bug Smash Fund</a> to raise money that would support our developers finding and fixing bugs in our software and to conduct routine maintenance. Maintenance isn’t a flashy new feature, and that makes it less interesting to many traditional funders, but it’s what keeps the reliable stuff working--and with your support, we’ve closed 370 Bug Smash Fund tickets.</p>
<p>These bugs and issues ranged from anti-censorship development, testing, onion services, documentation and improvements, Tor Browser UX changes, and tooling for development. This work keeps Tor Browser, the Tor network, and the <a href="https://blog.torproject.org/strength-numbers-entire-ecosystem-relies-tor">many tools that rely on Tor</a> strong, safe, and running smoothly.</p>
<p>And there’s so much more we can accomplish. Thirty-seven tickets tagged <a href="https://gitlab.torproject.org/groups/tpo/-/issues?scope=all&amp;state=opened&amp;label_name[]=BugSmashFund">BugSmashFund</a> are still open, and as you know, a big part of building software is ensuring that you can address issues when you find them. As such, <strong>starting August 1, every donation we receive during the month of August will count towards the Bug Smash Fund 2021.</strong></p>
<p><a href="https://donate.torproject.org"><img alt="A link to the Tor Project's donate page" src="/static/images/blog/inline-images/tor-donate-button_14.png" class="align-center" /></a></p>
<p>Your donation today to the Bug Smash Fund will help us:</p>
<ul>
<li><strong>Find and fix security bugs</strong>. We can’t predict when bugs will happen, but it is inevitable that issues will arise. Our top priority is to smash these bugs as quickly as possible when they are discovered.</li>
<li><strong>Push emergency releases</strong>. Past examples include: Emergency security update to Tor Browser to handle a mistake in Mozilla’s signing infrastructure. (<a href="https://blog.torproject.org/noscript-temporarily-disabled-tor-browser">‘NoScript Temporarily Disabled in Tor Browser,’ 2019</a>) and emergency release of Tor Browser, fixing an IP address leak issue caused by a bug in Firefox’s handling of file:// URLs. (<a href="https://blog.torproject.org/tor-browser-709-released">‘Tor Browser 7.0.9 is released,’ 2017</a>)</li>
<li><strong>Respond in the censorship arms race.</strong> We use the Bug Smash Fund to ensure that all people who need Tor can access it. This year, when Microsoft announced that it would shut down domain fronting attempts in Azure, <a href="https://lists.torproject.org/pipermail/anti-censorship-team/2021-April/000159.html">we used the Bug Smash Fund to set up domain fronting with Fastly for Snowflake and Moat.</a></li>
</ul>
<p>There are many different ways to contribute to the Bug Smash Fund, and all of them count towards reaching this goal:</p>
<ul>
<li><a href="https://donate.torproject.org/">Make a one-time donation</a> (and get swag like Tor stickers and t-shirts in return)</li>
<li><a href="https://donate.torproject.org/cryptocurrency/">Donate in ten different cryptocurrencies</a></li>
<li><a href="https://donate.torproject.org/monthly-giving/">Become a monthly donor and get your own Defenders of Privacy patch</a></li>
<li><a href="https://donate.torproject.org/champions-of-privacy/">Make a contribution of $1,000 and join the major donor group Champions of Privacy</a></li>
<li><a href="https://opencollective.com/thetorproject">Transfer your Open Collective gift card</a></li>
<li>Use the hashtag #TorBugSmash to share the campaign</li>
</ul>
<p>Your support keeps Tor strong. Thank you for being part of the fight for privacy online.</p>

