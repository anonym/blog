title: Tor 0.2.8.4-rc is released!
---
pub_date: 2016-06-15
---
author: nickm
---
tags:

tor
release
release candidate
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.8.4-rc has been released! You can download the source from the Tor website. Packages should be available over the next week or so.</p>

<p>Tor 0.2.8.4-rc is the first release candidate in the Tor 0.2.8 series. If we find no new bugs or regressions here, the first stable 0.2.8 release will be identical to it. It has a few small bugfixes against previous versions.</p>

<p>PLEASE NOTE: This is a release candidate. We think that we solved all<br />
of the showstopper bugs, but crucial bugs may remain. Please only run<br />
this release if you're willing to test and find bugs. If no<br />
showstopper bugs are found, we'll be putting out 0.2.8.5 as a stable<br />
release.</p>

<h2>Changes in version 0.2.8.4-rc - 2016-06-15</h2>

<ul>
<li>Major bugfixes (user interface):
<ul>
<li>Correctly give a warning in the cases where a relay is specified by nickname, and one such relay is found, but it is not officially Named. Fixes bug <a href="https://bugs.torproject.org/19203" rel="nofollow">19203</a>; bugfix on 0.2.3.1-alpha.
  </li>
</ul>
</li>
<li>Minor features (build):
<ul>
<li>Tor now builds once again with the recent OpenSSL 1.1 development branch (tested against 1.1.0-pre5 and 1.1.0-pre6-dev).
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the June 7 2016 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Cause the unit tests to compile correctly on mingw64 versions that lack sscanf. Fixes bug <a href="https://bugs.torproject.org/19213" rel="nofollow">19213</a>; bugfix on 0.2.7.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (downloading):
<ul>
<li>Predict more correctly whether we'll be downloading over HTTP when we determine the maximum length of a URL. This should avoid a "BUG" warning about the Squid HTTP proxy and its URL limits. Fixes bug <a href="https://bugs.torproject.org/19191" rel="nofollow">19191</a>.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-186896"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-186896" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2016</p>
    </div>
    <a href="#comment-186896">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-186896" class="permalink" rel="bookmark">What changes should be made</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What changes should be made to the configuration of a running v0.2.7.6 relay when upgrading to v0.2.8.4+?</p>
<p>Put another way, can I just upgrade the code and forget it, or are there other changes I can/should make to accommodate the newer version?</p>
<p>Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-186929"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-186929" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2016</p>
    </div>
    <a href="#comment-186929">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-186929" class="permalink" rel="bookmark">hurray &lt;3 - lets all go</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hurray &lt;3 - lets all go 0.2.8 asap</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-187036"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187036" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2016</p>
    </div>
    <a href="#comment-187036">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187036" class="permalink" rel="bookmark">Is the Maxmind GeoLite2</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is the Maxmind GeoLite2 Country database interchangeable between<br />
Tor/TBBs?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-187192"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187192" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 17, 2016</p>
    </div>
    <a href="#comment-187192">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187192" class="permalink" rel="bookmark">A good privacy improvement</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A good privacy improvement is using a random amount (min 1+3(for Bridge users - 1+2), max user-defined) of circuits.</p>
<p>We need to take in-to account that a large list of Exit nodes can be run by a single entity.</p>
<p>Imagine the following scenario:<br />
A single entity is running 100 Exit nodes. A Tor Browser user accesses blog.torproject.org, if Tor Browser user is unlucky(we all have chance on the default settings where we don't use Bridge) he is using only Nodes from the single entity. The single entity now can connect the dots and know the IP address and the browser address of Tor Browser user because he is the Entry (unless we use Bridge), The first, the second and the Exit node just because on the single fact that default users always use minimum and maximum 4 nodes.</p>
<p>If we use a Bridge then the single entity can only get the IP address of bridge. This is usually improved in case of Hidden Services that use multiple Bridges.</p>
<p>Conclusion:<br />
Even with more circuits the scenario can and will happen but will never be 100% sure because they don't know the circuit count.</p>
<p>Thank you for reading and please take this in to consideration. Can't wait to read your answer.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-187256"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187256" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 17, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-187192" class="permalink" rel="bookmark">A good privacy improvement</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-187256">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187256" class="permalink" rel="bookmark">that possibility is known</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>that possibility is known since long time and torproject already did several actions&amp;fixes do minimize correlation threads! if you fear such a szenario cause you need very high safety you should of cause add some care to additional protect yourself, like using public accesspoint&amp;machine and such.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-187533"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187533" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 19, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-187192" class="permalink" rel="bookmark">A good privacy improvement</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-187533">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187533" class="permalink" rel="bookmark">Sure, the scenario you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sure, the scenario you outline is possible. But 100% surety is actually easy for your attacker if traffic from a compromised exit node (in a chain of compromised relays) is seen leaving the Tor network.</p>
<p>It's also already possible today to use more than 3 relays if you wish. You'll take a performance hit, but maybe you'll decide that's okay. Maybe it'll improve your chances of anonymity. Maybe not, too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-187603"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187603" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 20, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-187192" class="permalink" rel="bookmark">A good privacy improvement</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-187603">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187603" class="permalink" rel="bookmark">can you help me get on the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>can you help me get on the dark web/</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-187288"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187288" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 17, 2016</p>
    </div>
    <a href="#comment-187288">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187288" class="permalink" rel="bookmark">What makes you think that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What makes you think that rogue entities don't operate unlisted bridge relays?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-187310"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187310" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 17, 2016</p>
    </div>
    <a href="#comment-187310">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187310" class="permalink" rel="bookmark">Smart detection for passive</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Smart detection for passive sniffing in the Tor-network</p>
<p><a href="https://chloe.re/2016/06/16/badonions/" rel="nofollow">https://chloe.re/2016/06/16/badonions/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-187313"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187313" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 18, 2016</p>
    </div>
    <a href="#comment-187313">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187313" class="permalink" rel="bookmark">&quot;We need to take in-to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"We need to take in-to account that a large list of Exit nodes can be run by a single entity."</p>
<p>Feel free to document this assumption, i know it's possible but it really never did happen.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-189037"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-189037" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 29, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-187313" class="permalink" rel="bookmark">&quot;We need to take in-to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-189037">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-189037" class="permalink" rel="bookmark">Why would the rogue entity</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why would the rogue entity ever speak of this? lol...</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-187389"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187389" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 18, 2016</p>
    </div>
    <a href="#comment-187389">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187389" class="permalink" rel="bookmark">thanks!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-187586"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187586" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 19, 2016</p>
    </div>
    <a href="#comment-187586">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187586" class="permalink" rel="bookmark">great
barki@ninja-network:~$</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>great<br />
<a href="mailto:barki@ninja" rel="nofollow">barki@ninja</a>-network:~$ apt-get install tor<br />
E: Could not open lock file /var/lib/dpkg/lock - open (13: Permission denied)<br />
E: Unable to lock the administration directory (/var/lib/dpkg/), are you root?<br />
<a href="mailto:barki@ninja" rel="nofollow">barki@ninja</a>-network:~$ sudo apt-get install tor<br />
sudo: unable to resolve host ninja-network<br />
[sudo] password for barki:<br />
Reading package lists... Done<br />
Building dependency tree<br />
Reading state information... Done<br />
tor is already the newest version (0.2.7.6-1ubuntu1).<br />
tor set to manually installed.<br />
The following packages were automatically installed and are no longer required:<br />
  linux-headers-4.4.0-23 linux-headers-4.4.0-23-generic<br />
  linux-image-4.4.0-23-generic linux-image-extra-4.4.0-23-generic<br />
Use 'sudo apt autoremove' to remove them.<br />
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-187602"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187602" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 20, 2016</p>
    </div>
    <a href="#comment-187602">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187602" class="permalink" rel="bookmark">so this may sound like a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>so this may sound like a stupid question but I'm 20 years of age and lol I'm not really that smart with computers so my question is. do you have to know what you are doing to get on the dark web i mean i have downloaded to tor but it still won't let me on can anyone help or am i sol</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-187905"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187905" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 22, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-187602" class="permalink" rel="bookmark">so this may sound like a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-187905">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187905" class="permalink" rel="bookmark">You need to be 21.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You need to be 21.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-188804"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-188804" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 28, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-187602" class="permalink" rel="bookmark">so this may sound like a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-188804">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-188804" class="permalink" rel="bookmark">A good starting point is The</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A good starting point is The Hidden Wiki. You should disable javascript first before you visit hidden websites.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-190469"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-190469" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 07, 2016</p>
    </div>
    <a href="#comment-190469">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-190469" class="permalink" rel="bookmark">Is there a windoze build we</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there a windoze build we can test?</p>
</div>
  </div>
</article>
<!-- Comment END -->
