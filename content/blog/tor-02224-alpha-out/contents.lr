title: Tor 0.2.2.24-alpha is out
---
pub_date: 2011-04-13
---
author: erinn
---
tags:

tor
bug fixes
alpha release
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.2.24-alpha fixes a variety of bugs, including a big bug that<br />
  prevented Tor clients from effectively using "multihomed" bridges,<br />
  that is, bridges that listen on multiple ports or IP addresses so users<br />
  can continue to use some of their addresses even if others get blocked.</p>

<p><a href="https://www.torproject.org/download/download" rel="nofollow">https://www.torproject.org/download/download</a></p>

<p>  <strong>Major bugfixes:</strong></p>

<ul>
<li>Fix a bug where bridge users who configure the non-canonical<br />
      address of a bridge automatically switch to its canonical<br />
      address. If a bridge listens at more than one address, it should be<br />
      able to advertise those addresses independently and any non-blocked<br />
      addresses should continue to work. Bugfix on Tor 0.2.0.x. Fixes<br />
      bug 2510.</li>
<li>If you configured Tor to use bridge A, and then quit and<br />
      configured Tor to use bridge B instead, it would happily continue<br />
      to use bridge A if it's still reachable. While this behavior is<br />
      a feature if your goal is connectivity, in some scenarios it's a<br />
      dangerous bug. Bugfix on Tor 0.2.0.1-alpha; fixes bug 2511.</li>
<li>Directory authorities now use data collected from their own<br />
      uptime observations when choosing whether to assign the HSDir flag<br />
      to relays, instead of trusting the uptime value the relay reports in<br />
      its descriptor. This change helps prevent an attack where a small<br />
      set of nodes with frequently-changing identity keys can blackhole<br />
      a hidden service. (Only authorities need upgrade; others will be<br />
      fine once they do.) Bugfix on 0.2.0.10-alpha; fixes bug 2709.</li>
</ul>

<p>  <strong>Minor bugfixes:</strong></p>

<ul>
<li>When we restart our relay, we might get a successful connection<br />
      from the outside before we've started our reachability tests,<br />
      triggering a warning: "ORPort found reachable, but I have no<br />
      routerinfo yet. Failing to inform controller of success." This<br />
      bug was harmless unless Tor is running under a controller<br />
      like Vidalia, in which case the controller would never get a<br />
      REACHABILITY_SUCCEEDED status event. Bugfix on 0.1.2.6-alpha;<br />
      fixes bug 1172.</li>
<li>Make directory authorities more accurate at recording when<br />
      relays that have failed several reachability tests became<br />
      unreachable, so we can provide more accuracy at assigning Stable,<br />
      Guard, HSDir, etc flags. Bugfix on 0.2.0.6-alpha. Resolves bug 2716.<br />
    - Fix an issue that prevented static linking of libevent on<br />
      some platforms (notably Linux). Fixes bug 2698; bugfix on<br />
      versions 0.2.1.23/0.2.2.8-alpha (the versions introducing<br />
      the --with-static-libevent configure option).</li>
<li>We now ask the other side of a stream (the client or the exit)<br />
      for more data on that stream when the amount of queued data on<br />
      that stream dips low enough. Previously, we wouldn't ask the<br />
      other side for more data until either it sent us more data (which<br />
      it wasn't supposed to do if it had exhausted its window!) or we<br />
      had completely flushed all our queued data. This flow control fix<br />
      should improve throughput. Fixes bug 2756; bugfix on the earliest<br />
      released versions of Tor (svn commit r152).</li>
<li>Avoid a double-mark-for-free warning when failing to attach a<br />
      transparent proxy connection. (We thought we had fixed this in<br />
      0.2.2.23-alpha, but it turns out our fix was checking the wrong<br />
      connection.) Fixes bug 2757; bugfix on 0.1.2.1-alpha (the original<br />
      bug) and 0.2.2.23-alpha (the incorrect fix).</li>
<li>When warning about missing zlib development packages during compile,<br />
      give the correct package names. Bugfix on 0.2.0.1-alpha.</li>
</ul>

<p>  <strong>Minor features:</strong></p>

<ul>
<li>Directory authorities now log the source of a rejected POSTed v3<br />
      networkstatus vote.</li>
<li>Make compilation with clang possible when using<br />
      --enable-gcc-warnings by removing two warning optionss that clang<br />
      hasn't implemented yet and by fixing a few warnings. Implements<br />
      ticket 2696.</li>
<li>When expiring circuits, use microsecond timers rather than<br />
      one-second timers. This can avoid an unpleasant situation where a<br />
      circuit is launched near the end of one second and expired right<br />
      near the beginning of the next, and prevent fluctuations in circuit<br />
      timeout values.</li>
<li>Use computed circuit-build timeouts to decide when to launch<br />
      parallel introduction circuits for hidden services. (Previously,<br />
      we would retry after 15 seconds.)</li>
</ul>

<p>  <strong>Packaging fixes:</strong></p>

<ul>
<li>Create the /var/run/tor directory on startup on OpenSUSE if it is<br />
      not already created. Patch from Andreas Stieger. Fixes bug 2573.</li>
</ul>

<p>  <strong>Documentation changes:</strong></p>

<ul>
<li>Modernize the doxygen configuration file slightly. Fixes bug 2707.</li>
<li>Resolve all doxygen warnings except those for missing documentation.<br />
      Fixes bug 2705.</li>
<li>Add doxygen documentation for more functions, fields, and types.</li>
</ul>

