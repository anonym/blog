title: New Alpha Release: Tor Browser 11.5a6 (Windows/macOS/Linux)
---
pub_date: 2022-03-11
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5a6 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a6 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a6/).

This version includes important security updates to Firefox:
- https://www.mozilla.org/en-US/security/advisories/mfsa2022-09/
- https://www.mozilla.org/en-US/security/advisories/mfsa2022-11/

Tor Browser 11.5a6 updates Firefox on Windows, macOS, and Linux to 91.7.0esr.

We use the opportunity as well to update various other components of Tor Browser:
- Tor 0.4.7.4-alpha
- NoScript 11.3.7

We also switch to the latest Go version (1.17.8) for building our Go-related projects.

The full changelog since [Tor Browser 11.5a4](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- Windows + OS X + Linux
  - Update Tor to 0.4.7.4-alpha
  - Update NoScript to 11.3.7
  - [Bug tor-browser#19850](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/19850): Disable Plaintext HTTP Clearnet Connections
  - [Bug tor-browser-build#40440](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40440): Bump version of snowflake to include PT LOG events
  - [Bug tor-browser#40819](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40819): Update Firefox to 91.7.0esr
  - [Bug tor-browser#40824](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40824): Drop 16539 Patch (android screencasting disable)
  - [Bug tor-browser#40825](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40825): Redirect HTTPS-Only error page when not connected
  - [Bug tor-browser#40826](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40826): Cherry-pick fixes for Mozilla bug 1758062
- Build System
  - Windows + OS X + Linux
    - Update Go to 1.17.8
    - [Bug tor-browser-build#40441](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40441): Add Austin as a valid git-tag signer
