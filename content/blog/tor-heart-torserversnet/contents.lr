title: Tor at the Heart: Torservers.net
---
pub_date: 2016-12-03
---
author: ssteele
---
tags:

heart of Internet freedom
Torservers
---
_html_body:

<p><i>During the month of December, we're highlighting other organizations and projects that rely on Tor, build on Tor, or are accomplishing their missions better because Tor exists. Check out our blog each day to learn about our fellow travelers. And please support the Tor Project! We're at the heart of Internet freedom.</i><br />
<a href="https://torproject.org/donate/donate-blog3" rel="nofollow">Donate today</a>!</p>

<p><b>Torservers.net</b></p>

<p>The <a href="https://torservers.net/" rel="nofollow">torservers.net organizational network</a> currently consists of 20 non-profit organizations in 14 countries that have joined forces to turn donations into Tor exit bandwidth. Each of the organizations participates in local and global events to teach others about what they have learned and to exchange knowledge on what it means to run Tor relays, specifically exit relays. </p>

<p>In close partnership with The Tor Project Inc., member organizations test new experimental releases, contribute to research at universities, and host Tor user meetings in their areas. Torservers.net has worked with a number of lawyers to produce legal assessments and publish guidelines for how to deal with complaints. In some cases, torservers.net covers legal costs for exit operators when needed. Members contribute to Tor and its codebase in many ways. For anyone interested in Tor, reaching out to a local Torservers.net organization is a very good way to connect to Tor folks!</p>

<p>Member organizations:</p>

<p><a href="https://www.privacyfoundation.at/" rel="nofollow">Austrian Privacy Foundation</a> (Austria)<br />
<a href="https://awp.is/" rel="nofollow">Associated Whistleblowing Press</a> (Belgium)<br />
<a href="https://coldhak.ca/" rel="nofollow">Coldhak</a> (Canada)<br />
<a href="https://www.koumbit.org/en" rel="nofollow">Koumbit</a> (Canada)<br />
<a href="https://effi.org/" rel="nofollow">Electronic Frontier Finland</a> (Finland)<br />
<a href="https://nos-oignons.net/" rel="nofollow">Nos Oignons</a> (France)<br />
<a href="https://saveyourprivacy.net/" rel="nofollow">SaveYourPrivacy e.V.</a> (Germany)<br />
<a href="https://www.zwiebelfreunde.de/" rel="nofollow">Zwiebelfreunde e.V.</a> (Germany)<br />
<a href="https://icetor.is/" rel="nofollow">IceTor</a> (Iceland)<br />
<a href="https://onionitalia.wordpress.com/" rel="nofollow">Onion Italia</a> (Italy)<br />
<a href="https://www.dfri.se/" rel="nofollow">DFRI: Föreningen för Digitala Fri- och Rättigheter</a> (Sweden)<br />
<a href="https://www.privacyfoundation.ch/" rel="nofollow">Swiss Privacy Foundation</a> (Switzerland)<br />
<a href="https://www.cyber-arabs.com/" rel="nofollow">Cyber Arabs (Institute for War &amp; Peace Reporting)</a> (Lebanon)<br />
<a href="https://enn.lu/" rel="nofollow">Frënn vun der Ënn</a> (Luxembourg)<br />
<a href="https://www.hartvoorinternetvrijheid.nl/" rel="nofollow">Hart voor Internetvrijheid</a> (Netherlands)<br />
<a href="https://www.accessnow.org/" rel="nofollow">Access Now</a> (USA)<br />
<a href="https://cypherchaikhana.org/" rel="nofollow">CypherChaikhana</a> (USA)<br />
<a href="https://www.calyxinstitute.org/" rel="nofollow">The Calyx Institute</a> (USA)<br />
<a href="https://libraryfreedomproject.org/" rel="nofollow">The Library Freedom Project</a> (USA)<br />
<a href="http://noisetor.net/" rel="nofollow">NoiseTor (Noisebridge)</a> (USA)</p>

---
_comments:

<a id="comment-223881"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223881" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 03, 2016</p>
    </div>
    <a href="#comment-223881">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223881" class="permalink" rel="bookmark">Thanks for this post! How</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for this post! How many % of the Tor total advertised exit bandwidth does torservernet manage?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-223916"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223916" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 03, 2016</p>
    </div>
    <a href="#comment-223916">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223916" class="permalink" rel="bookmark">I only use bridges that have</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I only use bridges that have Zwiebelfreunde in their whois. Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-223926"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223926" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 04, 2016</p>
    </div>
    <a href="#comment-223926">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223926" class="permalink" rel="bookmark">i do not see none operating</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i do not see none operating system and none free dns and none email provider and none isp or mesh organization and none political movement and none bank : are they anonymously included ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224023"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224023" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 05, 2016</p>
    </div>
    <a href="#comment-224023">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224023" class="permalink" rel="bookmark">I thought this was not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I thought this was not really funny so I had to share.  On the site access.now the link to download the brochure called: "A first look at digital security" requires java script enabled.  After you enable the scripts the link is: <a href="https://www.accessnow.org/cms/assets/uploads/2016/11/A-first-look-at-digital-security.pdf" rel="nofollow">https://www.accessnow.org/cms/assets/uploads/2016/11/A-first-look-at-di…</a>  Then a pop-up appeared telling me to share with social media.  How can the culture of digital security go from theory to actual web-site creativity (practice)?  How can you advocate digital privacy through "social media" and "corporate giants" being inside your site gathering information on your visitors.  Maybe within the tor community there should be a self-organized tor-educational campaign on the dos and donts of digital security.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-225111"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225111" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 14, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224023" class="permalink" rel="bookmark">I thought this was not</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-225111">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225111" class="permalink" rel="bookmark">It&#039;s terrible that NSA has</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's terrible that NSA has been so successful in "shaping" the Internet to ensure that anyone trying to design a webpage effectively has to choose between using unsafe commercial services such as Mailchimp or trying to design their own software (and probably making horrid security goofs in the process).</p>
<p>One can only hope that more activists will be able to receive security training from trusted sources such as FOTP, and that eventually more activist websites will be much more secure.  Once Tor Messenger gets out of beta, it may be much easier for activists to communicate less unsafely than is currently the case.</p>
<p>Thanks to Torservers for running all those much needed nodes!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-224035"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224035" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 05, 2016</p>
    </div>
    <a href="#comment-224035">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224035" class="permalink" rel="bookmark">&gt; the culture of digital</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; the culture of digital security<br />
&gt; I thought this was not really funny so I had to share.<br />
harceling,spreading, promoting, explaining, convincing,discussing,mailing,posting about few simple points :<br />
https - set your site in https<br />
-encryption - replace your obsolete protocols by the new standards<br />
-tweaks your config following the security announces<br />
-use non-us e-mail provider<br />
-avoid uk server<br />
-avoid vpn based in the five eyes<br />
-do not write your e-mail in the forum:blog<br />
-do not allow familiarity<br />
- and so many other subjects ... which use Tor ...<br />
The result was flame, silence, troll, spam, intellectual deficiency request, denunciation, extortion, hack etc.<br />
The culture of digital security begins by the first principle i read too quickly : educate yourself ; no one can do it for yourself. ,,.<br />
&gt; I thought this was not really funny so I had to share.<br />
i stopped my activist campaign &amp; all is fine now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
