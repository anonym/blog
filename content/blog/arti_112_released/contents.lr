title: Arti 1.1.2 is released: HsDir groundwork and cell handling improvements
---
author: nickm
---
pub_date: 2023-02-28
---
categories: announcements
---
summary:

Arti 1.1.2 is released and ready for download.
---
body:

Arti is our ongoing project to create a next-generation Tor client in
Rust.  At the start of this month, we released [Arti 1.1.1].  Now we're
announcing the next release in its series, Arti 1.1.2.

Since our last release, our primary focus has been preparation for onion
service support in Arti.  Since the last release, we've implemented the
[parsing logic] for the various layers of onion service descriptors, and
the various computations needed to maintain a [directory ring] to
decide where to find those descriptors.

While preparing for the next stages of onion service work, we found a
few deficiencies in the way that we handle incoming relay messages,
especially on streams. We've refactored that code significantly to
make it more [efficient] and [correct].

There have been many smaller changes as well; for those, please see the
[CHANGELOG].

For more information on using Arti, see our top-level [README], and the
docmentation for the [`arti` binary].

Thanks to everyone who has contributed to this release, including
Dimitris Apostolou, Emil Engler, and Shady Katy.

Also, our deep thanks to [Zcash Community Grants] for funding the
development of Arti!


[Arti 1.1.1]: https://blog.torproject.org/arti_111_released/
[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md#arti-112-28-february-2023
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[parsing logic]: https://tpo.pages.torproject.net/core/doc/rust/tor_netdoc/doc/hsdesc/index.html
[directory ring]: https://tpo.pages.torproject.net/core/doc/rust/tor_netdir/struct.NetDir.html#method.onion_service_dirs
[efficient]: https://gitlab.torproject.org/tpo/core/arti/-/commit/ca3b33a1afc58b84cc7a39ea3845a82f17cee0da
[correct]: https://gitlab.torproject.org/tpo/core/arti/-/issues/774
