title: Explore Tor, NYC! Meetup and Q&A On Feb 15
---
pub_date: 2018-01-29
---
author: steph
---
tags:

meetup
nyc
---
categories: community
---
summary:

Instead of looking at any single aspect of the Tor community, this meeting will feature a Q&A with Roger Dingledine and Alison Macrina. 
---
_html_body:

<p> </p>
<p>The Tor community is vast and deep yet remains a virtual entity outside periodic physical events. Last December, we opted to change that. Continuing the thread launched by the <a href="https://blog.torproject.org/explore-tor-new-york-city-new-meetup-starting-dec-7">Dec 7 meeting</a>, we have another Tor event set for New York City on Thursday, February 15th.</p>
<p>Instead of looking at any single aspect of the Tor community, this meeting will feature a Q&amp;A with Roger Dingledine and Alison Macrina. Roger, a Tor Co-Founder, and Alison, Community Team Lead, are both central to Tor development, and together they have a crisp picture of the project as a whole. They can often be found answering questions <a href="https://www.torproject.org/about/contact.html.en#irc">on IRC</a> or conducting presentations for the Tor-curious, and Feb 15th, we’ll have them both live in person in NYC.</p>
<p>Like the initial meeting, this event will start with an introduction by the speakers, followed up by question-and-answer time. If you're curious about any particular aspect of Tor, this event is for you. </p>
<p>We're looking forward to an engaging discussion where everyone leaves a little more knowledgeable.</p>
<p>The event will be held:</p>
<p>Feb 15, 2018 @ 6:45pm<br />
NYU's Tandon School of Engineering<br />
5 Metrotech Center Room LC-400<br />
Brooklyn, NY 11201</p>
<p>We’ll gather at Circa Brewing Co, 141 Lawrence St, Brooklyn, NY 11201 after the event to continue our dialogue.</p>
<p>There is no admission fee nor RSVP required for this event.</p>
<p>Join the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/regional-nyc">regional-nyc</a> mailing list to get notifications of upcoming NYC events in your inbox and stay connected with the locals online, too.</p>
<p>See you there. </p>

---
_comments:

<a id="comment-273779"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273779" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>nyc_feb15 (not verified)</span> said:</p>
      <p class="date-time">January 29, 2018</p>
    </div>
    <a href="#comment-273779">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273779" class="permalink" rel="bookmark">could a nyc resident answer…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>could a nyc resident answer at these informal questions ?<br />
- is the tor status a public or a private organization (an unknown sect/missing persons program) ?<br />
- are the users under the u.s jurisdiction when they run/surf on Tor/onions (torblog is in the usa &amp; ddg too)?<br />
- are we a 'virtual' justification for your exemption (taxes)?<br />
- if we are virtual american citizen ; what is the reason why we are not protected by the same laws than yourself (freedom of speech) ?<br />
- if we are anonymous citizens who do not need to be 'registered' but profit of your labor, time, money why are you working for us (motivation) ?<br />
- does the 501(3) allow you to be outside of the trouble of a normal life, to not be concerned ?<br />
- why the mailing-list (any) are not set as onion-service (email-onions-service) ?<br />
- are onion-blog-social-service made for "eyes only" &amp; surf (posting could reveal your identity) ?<br />
- why i cannot find a pizza, a stamp 'Tor' : easy to do, cheap, legal &amp; could show you how many supporters you have (revenue) ?<br />
- why the persons who are involved in the tor-projects post hostile/offensive contents on the tor-blog (impostors) ?<br />
- NYC is a modern town , is tor compatible with an anonymous powerful mesh network (wifi_public place like u/libraries/mcdonald are not at all anonymous) ?<br />
- the logo is not designed with art &amp; not so much symbolic : why do you not replace it ?<br />
- tor-team excluded (&amp; volunteers of course), does a stable tor-community exist ?<br />
- what is the reason why when i speak about tor or encryption(pgp) a silence &amp; the contempt happen as answer ?<br />
- will you provide gratis onions-soup at the meetup ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273792"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273792" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>DARKSTARS (not verified)</span> said:</p>
      <p class="date-time">January 30, 2018</p>
    </div>
    <a href="#comment-273792">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273792" class="permalink" rel="bookmark">@nonymousNews CAN SOMEONE…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@nonymousNews CAN SOMEONE TELL ME WHAT IS REALLY GOING ON IN THE US, WE NEED HELP IT'S NOT LOOKING GOOD FOR THE PEOPLE IN THE WORLD IF THIS KEEP GOING ON. PEOPLE REALLY NEED TO KNOW ABOUT TRUMP AND THE EMAIL FROM RUSSIA @DonaldJTrumpJr</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274019"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274019" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 19, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273792" class="permalink" rel="bookmark">@nonymousNews CAN SOMEONE…</a> by <span>DARKSTARS (not verified)</span></p>
    <a href="#comment-274019">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274019" class="permalink" rel="bookmark">History, they say, is never…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>History, they say, is never circular, but it is often helical.  It follows that in order to understand current events, it is almost always useful to revisit past events.</p>
<p>For example, to understand FBI's current group think (which explain why it devotes so much more effort to investigating alleged political "crimes" than to actual crimes, and to clumsy "suasion" programs intended to discourage "problematic" behavior, such as reading too much), you should know about past FBI activities and group think.  </p>
<p>Case in point: consider the following FBI dragnet surveillance program (which was revived-- with slightly different targets-- in technologically supercharged fashion after 9/11):</p>
<p>theatlantic.com<br />
The FBI's War on Black-Owned Bookstores<br />
At the height of the Black Power movement, the Bureau focused on the unlikeliest of public enemies: black independent booksellers.<br />
Joshua Clark Davis</p>
<p>&gt; The FBI conducted investigations of such black booksellers as ... Paul Coates in Baltimore (the father of The Atlantic national correspondent Ta-Nehisi Coates)...<br />
&gt; ...<br />
&gt; The FBI’s reports on black booksellers were highly invasive but often mundane. The FBI reports note phone calls from Coates’s number to his former comrades in the Black Panther Party—but also to Viking Press and the American Booksellers Association.<br />
&gt; ...<br />
&gt; Plainclothes agents who visited the store aroused employees’ suspicions when they sat in parked cars in front of the business for hours. In another incident, two men wearing suits who appeared to be federal agents visited Drum and Spear and asked to purchase the store’s entire inventory of Mao’s Little Red Book.<br />
&gt; ...<br />
&gt; While perhaps not surprising, it is deeply disturbing that Hoover and the FBI would carry out sustained investigations of black-owned independent bookstore across the country as part of COINTELPRO’s larger attacks on the Black Power movement. But Hoover’s order that agents track these stores’ customers represented not just an attack on black activists, but also an absolute contempt for America’s stated values of freedom of speech and expression. Any citizen who stepped into a black-owned bookstore, it seemed, risked being investigated by federal law enforcement.</p>
<p>And to answer your question, here is one way of summarizing what's been happening lately in the USA:</p>
<p>During the Nixon administration, FBI helped to partially convert the War on Vietnam into a War on American social justice movements, e.g. the one led by Martin Luther King.  A few decades later, after the collapse of the Soviet Union, the Military Industrial Complex, seeking a new mission, became the Surveillance-Military Industrial Complex.  Inevitably, the technologically enabled GWOT (Global War on Terror) spawned NSA's "Collect it all, Own it all, Exploit it all" dragnet cyberwar against all non-USPERs.  Over the span of the next few years, inevitably, the GWOT morphed into a War on US (We the People).</p>
<p>And naturally, the authors of the War on US, the intelligence chiefs, are making millions in their new role as talking heads explaining to TV viewers, in momentary sound-bites, the evils of the Surveillance State, for example in enabling Turnkey Authoritarianism, or offering potent cyberweapons to "national adversaries" such as Russia.  They never mention that privacy advocates had been warning of these dangers for decades.  Nor that their entire careers have been devoted to lying, misdirection, and manipulation of popular beliefs--- a manifest truth which if popular media even pretended to engage in rational discourse, would presumably call into question the credibility of the former USIC chiefs.</p>
<p>So if you really want to know what's happening in America, visit the kind of bookstore which is surveilled by FBI, and read, read, read.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273803"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273803" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Carson  (not verified)</span> said:</p>
      <p class="date-time">January 30, 2018</p>
    </div>
    <a href="#comment-273803">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273803" class="permalink" rel="bookmark">This is going to be really…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is going to be really interesting, I'm sure there's going to be news articles about it don't know how legit they'll be but whatever. You guys are open as hell about Q and As when it comes to 'citizens', but when it comes to government agencies let's lawyer up. Lol.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273804"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273804" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 30, 2018</p>
    </div>
    <a href="#comment-273804">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273804" class="permalink" rel="bookmark">Very very glad to see the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very very glad to see the NYC meetups will continue!   Because NYC has such a large number of network engineers and hackers, and has also experienced some drastic oppressive measures (such as the mass arrest of inaugural protesters), this is one of the best places in USA to promote TP's work and values.</p>
<p>In the past I criticized Roger for one very bad hiring decision (I trust measures have been taken to ensure nothing like that ever happens again), but I've also expressed appreciation for the fact that he provides a certain continuity with the early days, an institutional memory which I think has genuine value.  Further, he is probably better than almost anyone else at explaining highly technical matters reasonably well for "ordinary" Tor users.  He's been quiet here for a long time so I'm relieved to hear he's still with TP.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273909"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273909" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>utter (not verified)</span> said:</p>
      <p class="date-time">February 07, 2018</p>
    </div>
    <a href="#comment-273909">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273909" class="permalink" rel="bookmark">something is happening with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>something is happening with ubuntu as well.. many of the command line functions are working improperly if at all.. the current level of attack against citizens seeking to learn more about the world is increasing.. on all fronts.  Discussions everywhere have been shut down. How about a meeting in New Orleans?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273993"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273993" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Werner Hunter (not verified)</span> said:</p>
      <p class="date-time">February 15, 2018</p>
    </div>
    <a href="#comment-273993">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273993" class="permalink" rel="bookmark">Some types of bridges don&#039;t…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Some types of bridges don't work for some days now. Only obfs4 and regular Tor are still working. What's this about?</p>
</div>
  </div>
</article>
<!-- Comment END -->
