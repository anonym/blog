title: New Release: Tails 5.19
---
pub_date: 2023-10-31
---
author: tails
---
categories:

partners
releases
---
summary:

You can now close a given Tor circuit from the Onion Circuits interface.
Also: Tor Browser 13.0.1, Thunderbird 115, and bonus goodies for SecureDrop.

---
body:

## New features

### Closing a Tor circuit from _Onion Circuits_

You can now close a given Tor circuit from the _Onion Circuits_ interface.
This can help replace a particularly slow Tor circuit or troubleshoot issues
on the Tor network.

### Addition of sq-keyring-linter

At the request of people who use [SecureDrop](https://securedrop.org/) to
provide secure whistleblowing platforms across the world, we added the [`sq-
keyring-linter`](https://tracker.debian.org/pkg/rust-sequoia-keyring-linter)
package. `sq-keyring-linter` improves the cryptographic parameters of PGP keys
stored in their airgapped machines.

## Changes and updates

  * Update _Tor Browser_ to [13.0.1](https://blog.torproject.org/new-release-tor-browser-1301).

  * Update the _Tor_ client to 0.4.8.7.

  * Update _Thunderbird_ to [115.4.1](https://www.thunderbird.net/en-US/thunderbird/115.4.1/releasenotes/).

  * Update the _Linux_ kernel to 6.1.55.

## Fixed problems

For more details, read our
[changelog](https://gitlab.tails.boum.org/tails/tails/-/blob/master/debian/changelog).

## Known issues

None specific to this release.

See the list of [long-standing
issues](https://tails.net/support/known_issues/).

## Get Tails 5.19

### To upgrade your Tails USB stick and keep your Persistent Storage

  * Automatic upgrades are available from Tails 5.0 or later to 5.19.

You can [reduce the size of the
download](https://tails.net/doc/upgrade/#reduce) of future automatic upgrades
by doing a manual upgrade to the latest version.

  * If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a [manual upgrade](https://tails.net/doc/upgrade/#manual).

### To install Tails on a new USB stick

Follow our installation instructions:

  * [Install from Windows](https://tails.net/install/windows/)
  * [Install from macOS](https://tails.net/install/mac/)
  * [Install from Linux](https://tails.net/install/linux/)
  * [Install from Debian or Ubuntu using the command line and GnuPG](https://tails.net/install/expert/)

The Persistent Storage on the USB stick will be lost if you install instead of
upgrading.

### To download only

If you don't need installation or upgrade instructions, you can download Tails
5.19 directly:

  * [For USB sticks (USB image)](https://tails.net/install/download/)
  * [For DVDs and virtual machines (ISO image)](https://tails.net/install/download-iso/)

## Support and feedback

For support and feedback, visit the [Support
section](https://tails.net/support/) on the Tails website.
