title: Lavoro da remoto e sicurezza personale
---
pub_date: 2020-03-26
---
author: ggus
---
tags:

community
The Tor Project 
domestic abuse
health
privacy
remote working
tor browser
---
categories:

applications
community
human rights
---
summary: I nostri consigli per rimanere connessi in modo sicuro, lavorare a distanza e proteggere la vostra privacy mentre siete lontani socialmente o in quarantena.
---
_html_body:

<div class="ace-line"><em>View this post in <a href="https://blog.torproject.org/remote-work-personal-safety">English (en)</a> | <a href="https://blog.torproject.org/trabalho-remoto-seguranca-pessoal">Portuguese (pt)</a> | <a href="https://blog.torproject.org/Conectadas-seguras-tiempos-cuarentena">Spanish (es).</a></em></div>
<div class="ace-line"> </div>
<div class="ace-line" id="magicdomid384"><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">Stiamo vivendo un momento preoccupante e delicato a livello mondiale. Tor e' un'organizzazione 100%-remote e internazione che sviluppa strumenti per la sicurezza online. Per questo, vorremmo condividere alcuni consigli su come lavorare da casa senza rinunciare al nostro diritto alla privacy e liberta' d'espressione.</span></div>
<div class="ace-line"> </div>
<div class="ace-line">
<div class="ace-line" id="magicdomid14"><span class="b"><b>Lavoro da casa</b></span></div>
<div class="ace-line" id="magicdomid15"> </div>
<div class="ace-line" id="magicdomid382"><span>Dal</span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">la sua nascita, <a href="https://www.torproject.org/about/history/">nel</a></span><a href="https://www.torproject.org/about/history/"><span> 2006</span></a><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">,</span><span> il Tor Project e la sua grande comunitá lavorano da remoto. </span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">C</span><span>erchiamo di usare sempre software open source (cioé libero) </span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">che condivida il nostro </span><span>impegno verso i diritt</span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">i</span><span> uman</span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">i</span><span> </span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">alla</span><span> privacy e libertá di espressione online. </span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">Questi sono alcuni strumenti che utiliziamo tutti i giorni per collaborare e comunicare online.</span></div>
<div class="ace-line" id="magicdomid17"> </div>
<div class="ace-line" id="magicdomid640"><span class="b"><b>IRC. </b></span><span>La maggior parte delle nostre conversazioni avvengono su canali IRC pubblici, come per esempio #tor-project, #tor-dev, and #tor-www e <a href="https://support.torproject.org/get-in-touch/irc-help/">tanti altri ancora</a>. Se vuoi parlare di Tor con noi puoi trovarci nel canale #tor. </span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">Qualsiasi software di chat la vostra organizzazione stia utilizzando, vi raccomandiamo di creare un canale "off-topic" o di utilizzare le chat private con le persone con cui volete comunicare. In questo modo avrete l'opportunita' di interaggire ad un livello piu' personale, condividendo notizie o chiaccherando senza interferire con gli altri canali. Non e' necessario condividere il vostro indirizzo email o altre informazioni personali per poter utilizzare IRC</span></div>
<div class="ace-line" id="magicdomid642"> </div>
<div class="ace-line" id="magicdomid20"> </div>
<div class="ace-line" id="magicdomid871"><a href="https://nextcloud.com/"><span class="b"><b>Nextcloud</b></span></a><span> Questa piattaforma puó essere un alternativa a G</span><span class="author-a-z81zbz79zz90zz71z5z82z9wz70zz78zcotm4">oogle</span><span> Suite. La usiamo per documenti collaborativi, calendario e per salvare file.</span></div>
<div class="ace-line" id="magicdomid22"> </div>
<div class="ace-line" id="magicdomid23"> </div>
<div class="ace-line" id="magicdomid649"><span class="b"><b><a href="https://pad.riseup.net/">Riseup pads</a>.</b></span><span> Usiamo questa applicazione come </span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">blocco note</span><span>, per prendere nota di qualcosa o semplicemente per fare bozze prima di pubblicare articoli sul blog. I pads non vengono salvati indefinitivamente perció non é adatto come storage.</span></div>
<div class="ace-line" id="magicdomid25"> </div>
<div class="ace-line" id="magicdomid26"><em><span>Per chi lavora con gruppi a rischio o con informazioni sensibili, i programmi che riportiamo possono essere molto utili.</span></em></div>
<div class="ace-line" id="magicdomid27"> </div>
<div class="ace-line" id="magicdomid28"><a href="https://torproject.org/download"><span class="b"><b>Tor Browser</b></span></a><span>. Usare Tor Browser per fare ricerche, accedere ai vostri account, o semplicemente per collaborazioni tra colleghi, vi protegge dal tracciamento dei siti web, dalla sorveglianza del vostro gestore internet, da qualcuno che potrebbe monitorare la vostra connessione, e dalle censure sia del vostro gestore internet o governo. Se vi occupate di salute o essere i primi a dover condurre ricerche per informazioni sensibili che potrebbero essere identificative per un individuo o per altre piu facili attivitá di monitoraggio, Tor Browser é un programma che puó proteggere le persone che state aiutando.</span></div>
<div class="ace-line" id="magicdomid29"> </div>
<blockquote><div class="ace-line" id="magicdomid657"><span class="b"><b>'Sono un dottore in un </b></span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd b"><b>paese a rischio</b></span><span class="b"><b>. Quando ho bisogno di fare ricerche su una malattia,trattamento o semplicemente per guardare la storia dei miei pazienti, sono a conoscenza che la cronologia del mio browser potrebbe essere correlata ai miei pazienti e che potrebbe rivelare informazioni del loro stato di salute,familiare e di vita personale.</b></span></div>
<div class="ace-line" id="magicdomid31"><span class="b"><b>Io uso Tor per la maggior parte delle mie ricerche quando credo che ci possa essere il rischio di correlazione con le visite dei pazienti.</b></span></div>
<div class="ace-line" id="magicdomid32"> </div>
<div class="ace-line" id="magicdomid33"><span class="b"><b>-Anonymous Tor User</b></span></div>
</blockquote>
<div class="ace-line" id="magicdomid34"> </div>
<div class="ace-line" id="magicdomid658"><a href="https://www.signal.org/"><span class="b"><b>Signal</b></span></a><span>. Noi la usiamo per conversazioni con singole persone, chiamate e piccoli gruppi di chat. E' open-source, i messaggi sono criptati ed é possibile dare un tempo di scadenza ai messaggi che contengono informazioni sensibli.</span></div>
<div class="ace-line" id="magicdomid36"> </div>
<div class="ace-line" id="magicdomid659"><span class="b"><b><a href="https://meet.jit.si/">Jitsi Meet</a>.</b></span><span> Per meeting e chiamate sia video che vocali. Jitsi é un ottimo programma. E' open-source, criptato, e non servono account per usufruirne. Basta digitare un nome alla chat (es: </span><span class="url"><a href="https://meet.jit.si/testchat" rel="noreferrer noopener">https://meet.jit.si/testchat</a></span><span> , e condividere il link con le persone con cui volete. Provate questo prima di <a href="https://www.accessnow.org/access-now-urges-transparency-from-zoom-on-privacy-and-security/">Zoom (programma simile) che al momento viene scrutinato</a> a causa della poca trasparenza di policy.</span></div>
<div class="ace-line" id="magicdomid38"> </div>
<div class="ace-line" id="magicdomid39"><span class="b"><b><a href="https://onionshare.org/">OnionShare</a>.</b></span><span> Questo programma permette di condividere file di qualsiasi "peso" in modo sicuro ed anonimo senza terze parti. Se avete bisogno di condividere file sensibili con una o piu persone, <a href="https://blog.torproject.org/new-version-onionshare-makes-it-easy-anyone-publish-anonymous-uncensorable-websites-0">l'ultima versione di OnionShare</a> permette anche la creazione di un indirizzo onion accessibile esclusivamente tramite la rete Tor.</span></div>
<div class="ace-line" id="magicdomid40"> </div>
<div class="ace-line" id="magicdomid41"><span class="b"><b><a href="https://share.riseup.net/">Share.riseup.net</a>.</b></span><span> Questo é un servizio web accessibile tramite browser per la veloce condivisione di piccoli file (fino a 50mb). Usiamo spesso questi link per condividere photo e screenshot nei nostri canali IRC.</span></div>
<div class="ace-line" id="magicdomid42"> </div>
<div class="ace-line" id="magicdomid43"> </div>
<div class="ace-line" id="magicdomid44"><span>Se ancora credete di non aver trovato il programma giusto alle vostre esigenze, anarcat, SysAdmin del Progetto Tor, ha altre raccomandazioni per <a href="https://anarc.at/blog/2020-03-15-remote-tools/">Programmi da remoto per le distanze sociali</a>.</span></div>
<div class="ace-line" id="magicdomid45"> </div>
<div class="ace-line" id="magicdomid46"><span class="b"><b>Sicurezza Personale</b></span></div>
<div class="ace-line" id="magicdomid47"> </div>
<div class="ace-line" id="magicdomid864"><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">Una </span><span>casa non é un posto sicuro per tutti.</span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd"> Siamo consapevoli che alcune persone sono</span><span> costrette a restare a casa</span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd"> o c</span><span>he possono avere relazioni con altri individui che potrebbero metterle a rischio. Se vi trovate in una situazione in cui avete bisogno d'aiuto oppure siete in contatto con persone che hanno bisogno di aiuto, raccomandiamo di usare Tor Browser per cercare </span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">informazioni o richiedere assistenza</span><span> senza </span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">dovervi preoccupare delle tracce lasciate nella vostra cronologia di navigazione</span><span>. </span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">La <a href="https://www.techsafety.org/digital-services-during-public-health-crises">rete</a></span><a href="https://www.techsafety.org/digital-services-during-public-health-crises"><span> Nazionale Contro la Violenza Domestica</span></a><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd"><a href="https://www.techsafety.org/digital-services-during-public-health-crises"> </a>degli Stati Uniti</span><span> puó fornire maggiori raccomandazioni da seguire</span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd"> in queste situazioni.</span></div>
<div class="ace-line" id="magicdomid49"> </div>
<div class="ace-line" id="magicdomid51"><span>La stessa raccomandazione sopracitata, la facciamo a chi si accinge a fare ricerche di dati personali sensibili, che riguardino la salute sia mentale che fisica e/o l'immigrazione.</span></div>
<div class="ace-line"> </div>
<div class="ace-line" id="magicdomid52"><span>L'uso di <a href="https://torproject.org/download">Tor Browser</a>, in combinazione con il motore di ricerca <a href="https://duckduckgo.com/">DuckDuckGo</a>, puo aiutare a tenere sicuri i tuoi dati personali, offrendo la possibilita di scegliere cosa condividere, e dando la possibilitá di accedere ad informazioni e risorse sensibili che potrebbero essere bloccate o sotto osservazione. </span></div>
<div class="ace-line" id="magicdomid53"> </div>
<div class="ace-line" id="magicdomid54"> </div>
<blockquote><div class="ace-line" id="magicdomid55"><span class="b"><b>Uso Tor Browser per far ricerche su disturbi mentali, come depressione, che accadono nelle nostre famiglie. Non voglio che nessuno venga a conoscenza di queste mie ricerche a menocché io non voglia dirglielo. Per questo uso Tor per fare ricerche riguardo tutto quello correlato con queste malattie.</b></span></div>
<div class="ace-line" id="magicdomid56"> </div>
<div class="ace-line" id="magicdomid57"><span class="b"><b>- Anonymous Tor User</b></span></div>
</blockquote>
<div class="ace-line" id="magicdomid58"> </div>
<div class="ace-line" id="magicdomid59"><span>Molti dei programmi che abbiamo riportato, come Jitsi, Signal, e OnionShare, possono aiutare per avere comunicazioni sicure in circostanze difficili.</span></div>
<div class="ace-line" id="magicdomid60"> </div>
<div class="ace-line" id="magicdomid61"> </div>
<div class="ace-line" id="magicdomid62"><span class="b"><b>Resta Connesso</b></span></div>
<div class="ace-line" id="magicdomid63"> </div>
<div class="ace-line" id="magicdomid867"><span>In questo momento </span><span class="author-a-qz70zuixh0z71zoilz77zaz88zz86zd">incerto</span><span> in cui viviamo, restare connessi é importantissimo e fare la nostra parte per salvaguardare sia noi che i nostri cari é vitale. Per qualsiasi domanda su Tor, o sui programmi che abbiamo presentato, e per come potrebbero aiutarvi potete entrare nel nostro canale #Tor in IRC.</span></div>
<div class="ace-line" id="magicdomid65"> </div>
<div class="ace-line" id="magicdomid66"><span>Se siete interessati a partecipare al nostro lavoro, diamo il benvenuto a tutti nella nostra comunitá.</span></div>
<div class="ace-line" id="magicdomid67"><span>Siamo una piccola organizzazione no-profit con una grande missione: portare la privacy e la libertá online come opzione di base, e questo lavoro á reso possibile grazie a migliaia di volontari sparsi in tutto il mondo. Il nostro <a href="https://blog.torproject.org/docshackathon-2020">secondo DocsHackathon</a>, un evento completamente svolto in remoto, inizierá questa Domenica 22 Marzo. Diamo un grande valore al vostro tempo, collaborazione e alla vostra presenza. Speriamo che vi uniate a noi.</span></div>
<div class="ace-line" id="magicdomid68"> </div>
<div class="ace-line" id="magicdomid69"><span>Siamo una delle tante comunitá online dove il vostro aiuto potrebbe essere molto importante, perció se non vi trovate bene con noi, non smettete di cercare. Questa potrebbe anche essere un opportunitá per iniziare una vostra comunitá.</span></div>
<div class="ace-line" id="magicdomid70"> </div>
<div class="ace-line" id="magicdomid71"><span>Da organizzazione no-profit, facciamo conto sul vostro supporto per continuare a mantenere la rete Tor accessibile a tutti. Fate una <a href="https://donate.torproject.org">donazione</a> se potete per aiutarci a riprenderci Internet. Se avete la possibilita' finanziaria, gli sviluppatori dei servizi sopracitati, incluso <a href="https://riseup.net/en/donate">Riseup</a>, potrebbero aver bisogno del vostro supporto. Anche una piccolissima offerta puo' fare la differenza.</span></div>
<div class="ace-line"> </div>
<div class="ace-line"><em>Traduzione di: AkuAq_</em></div>
</div>

