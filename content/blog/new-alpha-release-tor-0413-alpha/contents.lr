title: New alpha release: Tor 0.4.1.3-alpha
---
pub_date: 2019-06-25
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.1.3-alpha from the <a href="https://www.torproject.org/download/tor/">usual place on the website</a>. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely in the next couple of weeks.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.4.1.3-alpha resolves numerous bugs left over from the previous alpha, most of them from earlier release series.</p>
<h2>Changes in version 0.4.1.3-alpha - 2019-06-25</h2>
<ul>
<li>Major bugfixes (Onion service reachability):
<ul>
<li>Properly clean up the introduction point map when circuits change purpose from onion service circuits to pathbias, measurement, or other circuit types. This should fix some service-side instances of introduction point failure. Fixes bug <a href="https://bugs.torproject.org/29034">29034</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the June 10 2019 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/30852">30852</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (logging):
<ul>
<li>Give a more useful assertion failure message if we think we have minherit() but we fail to make a region non-inheritable. Give a compile-time warning if our support for minherit() is incomplete. Closes ticket <a href="https://bugs.torproject.org/30686">30686</a>.</li>
</ul>
</li>
<li>Minor bugfixes (circuit isolation):
<ul>
<li>Fix a logic error that prevented the SessionGroup sub-option from being accepted. Fixes bug <a href="https://bugs.torproject.org/22619">22619</a>; bugfix on 0.2.7.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (continuous integration):
<ul>
<li>Allow the test-stem job to fail in Travis, because it sometimes hangs. Fixes bug <a href="https://bugs.torproject.org/30744">30744</a>; bugfix on 0.3.5.4-alpha.</li>
<li>Skip test_rebind on macOS in Travis, because it is unreliable on macOS on Travis. Fixes bug <a href="https://bugs.torproject.org/30713">30713</a>; bugfix on 0.3.5.1-alpha.</li>
<li>Skip test_rebind when the TOR_SKIP_TEST_REBIND environment variable is set. Fixes bug <a href="https://bugs.torproject.org/30713">30713</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (directory authorities):
<ul>
<li>Stop crashing after parsing an unknown descriptor purpose annotation. We think this bug can only be triggered by modifying a local file. Fixes bug <a href="https://bugs.torproject.org/30781">30781</a>; bugfix on 0.2.0.8-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (pluggable transports):
<ul>
<li>When running as a bridge with pluggable transports, always publish pluggable transport information in our extrainfo descriptor, even if ExtraInfoStatistics is 0. This information is needed by BridgeDB. Fixes bug <a href="https://bugs.torproject.org/30956">30956</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Mention URLs for Travis/Appveyor/Jenkins in ReleasingTor.md. Closes ticket <a href="https://bugs.torproject.org/30630">30630</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-282832"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-282832" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>unknown (not verified)</span> said:</p>
      <p class="date-time">June 25, 2019</p>
    </div>
    <a href="#comment-282832">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-282832" class="permalink" rel="bookmark">hi. Tor website is blocked…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi. Tor website is blocked in <em>Iran</em> like many other websites. we Iranians use VPNs and anti-filters to overcome the censorship. i only use TOR but since the TOR website is censored too, if the TOR browser stops working any day (by increasingly malicious activities by our government) i will not be able to ask for help.<br />
so i'm asking now beforehand.<br />
is there any <em>email address</em> from you guys that if TOR browser stopped working, i could get in touch with you and you send me newest version of Tor-browser .exe file to <strong>download directly</strong> without going to TOR website and using TOR servers? (i mean you upload the whole .exe file on gmail server and send it to me). is it possible for you?<br />
please, please, please, answer this fast and also do it under an official ID that i could count on it as a true TOR official.<br />
thank you for all you have done for us through these years.<br />
a sincere fan</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-282848"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-282848" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Free Beer (not verified)</span> said:</p>
      <p class="date-time">June 26, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-282832" class="permalink" rel="bookmark">hi. Tor website is blocked…</a> by <span>unknown (not verified)</span></p>
    <a href="#comment-282848">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-282848" class="permalink" rel="bookmark">I am not a Tor official, but…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am not a Tor official, but this is what the GetTor section of the website says:</p>
<p>GetTor is a service that provides alternative methods to download the Tor Browser, especially for people living in places with high levels of censorship, where access to Tor Project's website is restricted.</p>
<p>The idea behind GetTor is very simple:</p>
<p>Step 1: Send a request to GetTor specifying your operating system (and optionally your locale).</p>
<p>Step 2: GetTor will send you back a reply with links to download Tor Browser from our supported providers.</p>
<p>Step 3: Download Tor Browser from one of the providers. When done, check the integrity of the downloaded files.</p>
<p>Step 4: If required, get some bridges!</p>
<p>You can make requests to GetTor using different channels of communication and different locales. At the present moment, we support the following locales: English (en), Farsi (fa), Chinese (zh), Turkish (tr), and the following channels:</p>
<p>Email: you can make a request sending an email to <a href="mailto:gettor@torproject.org" rel="nofollow">gettor@torproject.org</a> </p>
<p>Quick example:</p>
<p>To get links for downloading Tor Browser in Farsi for Windows, send an email to <a href="mailto:gettor+fa@torproject.org" rel="nofollow">gettor+fa@torproject.org</a> with the word windows in the body of the message.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-282905"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-282905" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 28, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-282832" class="permalink" rel="bookmark">hi. Tor website is blocked…</a> by <span>unknown (not verified)</span></p>
    <a href="#comment-282905">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-282905" class="permalink" rel="bookmark">https://tb-manual.torproject…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://tb-manual.torproject.org/downloading/" rel="nofollow">https://tb-manual.torproject.org/downloading/</a> - mirrors and GetTor<br />
<a href="https://support.torproject.org/#gettor" rel="nofollow">https://support.torproject.org/#gettor</a><br />
<a href="https://gettor.torproject.org/" rel="nofollow">https://gettor.torproject.org/</a><br />
<a href="http://tngjm3owsslo3wgo.onion/" rel="nofollow">http://tngjm3owsslo3wgo.onion/</a> (proof: <a href="https://onion.torproject.org/" rel="nofollow">https://onion.torproject.org/</a> )<br />
<a href="https://2019.www.torproject.org/docs/faq.html.en#GetTor" rel="nofollow">https://2019.www.torproject.org/docs/faq.html.en#GetTor</a><br />
<a href="https://blog.torproject.org/say-hi-new-gettor" rel="nofollow">https://blog.torproject.org/say-hi-new-gettor</a></p>
<p>Save the instructions in a text file. Always verify the PGP signatures of the files no matter from where you download it.</p>
<p>Adding to the other reply,<br />
To use GetTor via Twitter, send a direct message to <a href="https://twitter.com/get_tor" rel="nofollow">@get_tor</a> with one of the following codes in it (You don't need to follow the account): Linux, macOS (OS X), Windows.  For the Farsi Windows version, write "windows fa" without quotation marks.</p>
<p>To use GetTor via XMPP (Jitsi, CoyIM, etc.), send a message to <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #339933;">&lt;</span>a href<span style="color: #339933;">=</span><span style="color: #0000ff;">&quot;mailto:gettor@torproject.org&quot;</span> rel<span style="color: #339933;">=</span><span style="color: #0000ff;">&quot;nofollow&quot;</span><span style="color: #339933;">&gt;</span>gettor<span style="color: #339933;">@</span>torproject<span style="color: #339933;">.</span>org<span style="color: #339933;">&lt;/</span>a<span style="color: #339933;">&gt;</span></code></span> with one of the same codes.</p>
<p>After you install it, if Tor Browser is unable to connect to the Tor network because guard nodes are blocked, use <a href="https://bridges.torproject.org/" rel="nofollow">bridges</a>. If the bridges website is blocked, send an email to <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #339933;">&lt;</span>a href<span style="color: #339933;">=</span><span style="color: #0000ff;">&quot;mailto:bridges@torproject.org&quot;</span> rel<span style="color: #339933;">=</span><span style="color: #0000ff;">&quot;nofollow&quot;</span><span style="color: #339933;">&gt;</span>bridges<span style="color: #339933;">@</span>torproject<span style="color: #339933;">.</span>org<span style="color: #339933;">&lt;/</span>a<span style="color: #339933;">&gt;</span></code></span>. Please note that you must send the email using an address from one of the following email providers: Riseup or Gmail.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-282836"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-282836" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 26, 2019</p>
    </div>
    <a href="#comment-282836">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-282836" class="permalink" rel="bookmark">Hello, please, help me. I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello, please, help me. I have a Linux laptop (Endless 86_64) and want to run a free virtual machine (prefer a hypervisor) for commercial use. Highly important privacy.<br />
1. What a matches have? (If I right understood, Proxmox will delete all data from the host machine. No need.)<br />
2. Is there any matches without a GPL, Apache or like it license?<br />
3. What exactly goals have TAILS? What a negative points it have? It can be run without a USB-device?<br />
4. One of my problems is a question about running .exe files. If I right understood it dangerous to run Wine or PlayOnLinux without a virtual machine above it? (Is it enough to run that under a virtual machine?) What exactly malware are bonded with Wine and PlayOnLinux (<a href="https://en.wikipedia.org/wiki/Wine_(software)#Security" rel="nofollow">https://en.wikipedia.org/wiki/Wine_(software)#Security</a>)? What problems are possible if run as superuser? Is Wine and PlayOnLinux are legal programs? Wine is a free and open source, PlayOnLinux too? Is it legal to install PlayOnLinux? As I understood some programs that possible to install from laptop manager are illegal (VLC for example <a href="https://en.wikipedia.org/wiki/VLC_media_player#Legality" rel="nofollow">https://en.wikipedia.org/wiki/VLC_media_player#Legality</a>).<br />
5. Where I can anonymously ask any questions like that? (Better if without a registration and with Tor-safest enabled.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-282901"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-282901" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 28, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-282836" class="permalink" rel="bookmark">Hello, please, help me. I…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-282901">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-282901" class="permalink" rel="bookmark">&gt; 3. What exactly goals have…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; 3. What exactly goals have TAILS? What a negative points it have? It can be run without a USB-device?</p>
<p>I use Tails for almost everything but off-line in-home stuff, so I'll take a stab at that.</p>
<p>The goal of Tails is to provide a complete Debian based "amnesiac" system which works "out of the box" by booting Tails from a DVD or bootable USB.  Booting from a DVD is more secure but booting from a USB is more convenient because you can use Tails not only to create the bootable USB (once you are running Tails booted from a DVD) but to create a LUKS-encrypted "persistent volume" where you can store files, and also to download a few of the most useful applications (such as steganography or additional crypto tools) not included in default Tails, which will be installed (in RAM, i.e. in the virtual machine) next time you boot from the Tails USB from data stored in your persistent volume.</p>
<p>"Amnesiac" means Tails tries hard to avoid leaving any traces in hardware of your activity during a Tails session, which can be critically important for human rights workers, medical aid workers, journalists, union organizers, opposition politicians, corporate executives, diplomatic officers, oil/mineral explorers, doctors, cybersecurity researchers, LEA liasons, and others who may need to travel frequently, sometimes in dangerous countries, and who need to communicate or work while traveling without risking a dangerous adversary (often the host country government) from breaking down doors, snatching a laptop, and claiming to find evidence of "human trafficking" or whatever else is the locally favored method of attempting to discredit some NGO or political party.</p>
<p>Case in point: the dozens of individually identified "high value targets" (HVTs) who were targeted by what appears to be a sophisticated and long-running CN government cyberespionage program which entailed establishing covert APT presence inside the networks of dozens of international telecoms, apparently for the purpose of tracking metadata and possibly content of communications involving the HVTs, would very likely have been better served if they had used Tails and OnionShare rather than vulnerable smart phones and insecure telecom companies to enable them to stay in touch with their personal calling circle (for "legitimate" government business, conniving with corporate lobbyists, political scheming, plus managing bribery, kickbacks, money-laundering, off-shoring, tax-evasion, assassination plots, and other "business as usual" for high government officials--- or so one gathers from reading what is know so far about the covert activity of such malign figures as MBS and DJT).</p>
<p>The best way to obtain Tails is probably to download the latest ISO image from tails.boum.org (either via software like curl or via torrents), verify the detached signature, if possible verify the signing key using the Web of Trust, burn the DVD on a trustworthy (we hope) computer (i.e. not one which some nations customs people have stuck their dirty paws into), and then make disposable Tails USB sticks with persistent volumes to use while traveling with a disposable laptop-- possibly one bought with cash "in-country".  For example.</p>
<p>You can boot Tails in a more secure "off-line" mode when you are not planning to venture onto the internet, which can considerably enhance your security when you are writing story for your paper or a report on your mineral findings (for example), which you certainly would not want to fall into the hands of an adversary or competitor.  To this end, applications provided in Tails include Libreoffice, Gimp, and many other standard tools in the Linux arsenal, which can be used offline.</p>
<p>Tails also includes some networking tools such as airodump-ng which can be useful when you boot in on-line mode with WiFi enabled, for example to check out nearby WiFI access points.  In the ever evolving arms race between repressive governments and the courageous reporters who write about their ugly activities, it is becoming more common for reporters to avoid physically meeting their sources "in-country".  Rather one can arrange to be close enough for the source to log onto a temporary private WiFi AP the reporter has created for the solve purpose of communicating with the source.  Of course, the opposition must might be savvy enough to be looking for a strange private WiFi AP popping up when they observe someone they are physically following booting up their  laptop.  OnionShare is probably much safer.  As always, making the initial connection in order to set up communication is the most dangerous stage, so you should use counter-surveillance methods, which is where some of those extra networking apps can come in handy.  For example, targets of government surveillance can often intercept the unencrypted(!) video feeds of fixed surveillance cameras hidden inside "concealments", an increasingly common feature of many urban environments.  Watching them watch you on closed circuit TV is pretty good evidence that this is not the best moment to meet your source.</p>
<p>Tails has suffered in the past from some horrific security vulnerabilities, including a devastating bug in the shell ("Shellshock"), and in common with pretty much every OS it is likely vulnerable to newly discovered members of the seemingly vast family of attacks exploiting the inherent insecurity of "speculative execution" CPU architectures.  But these are rapidly fixed in new releases of Tails as soon as they are disclosed.  Which is alarming but there is really nothing out there which does any better.  Furthermore, we know from the Snowden leaks that as of 2012 NSA and GCHQ teams assigned to crack Tails were having a very hard time.  So evidently NSA did not know about Shellshock either, and hopefully learned of it the same way the rest of us did, when Google's cybersecurity lab revealed it.  That's very solid evidence that Tails is trustworthy (i.e. does not deliberately cooperate with spooks) and probably as secure as anything you are going to find anywhere, including very expensive proprietary commercial products which might very well have easily exploited holes.  At least with open source you have the chance that a white hat will spot a big problem before the particular black hats who are on your tail, no pun intended.</p>
<p>NGOs can sometimes obtain free security training from Tor Project or like minded organizations, I think, but for-profit companies should hire a reputable security consultant, budget permitting.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
