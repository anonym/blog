title: Trip report, 29c3
---
pub_date: 2013-01-10
---
author: arma
---
tags:

talks
advocacy
trip report
29c3
---
categories:

advocacy
community
reports
---
_html_body:

<p>Jacob and I did <a href="http://events.ccc.de/congress/2012/Fahrplan/events/5306.en.html" rel="nofollow">another Tor talk</a> at <a href="http://events.ccc.de/congress/2012/" rel="nofollow">CCC</a> this year. This time we focused on explaining all the various Tor project components (mostly development-related) that <a href="https://www.torproject.org/getinvolved/volunteer#Projects" rel="nofollow">need help</a> from the broader community.</p>

<p>The talk went well, but we were in the smaller room, and we and the conference organizers had failed to communicate that it was meant to be more of a workshopy atmosphere. We had a lot of people there who just wanted to see the sequel to our <a href="http://www.youtube.com/watch?v=GwMr8Xl7JMQ" rel="nofollow">spectacle</a> last year, and it meant we turned away many hundred Tor enthusiasts. Live and learn I guess. I did end up holding a post-talk Tor Q&amp;A session that lasted for seven hours.</p>

<p>I'm still patiently waiting for the official video to emerge [Edit: <a href="http://ftp.ccc.de/congress/2012/mp4-h264-HQ/29c3-5306-en-the_tor_software_ecosystem_h264.mp4" rel="nofollow">here it is!</a>], but in the meantime there's a <a href="http://www.youtube.com/watch?v=Rnbc_9JnVtc" rel="nofollow">youtube copy of the video stream</a>; you can look at <a href="http://freehaven.net/~arma/slides-29c3.odp" rel="nofollow">the</a> <a href="http://freehaven.net/~arma/slides-29c3.pdf" rel="nofollow">slides</a> too.</p>

<p>Some other highlights from Congress:</p>

<ul>
<li>Be sure to watch the <a href="http://www.youtube.com/watch?v=XDM3MqHln8U" rel="nofollow">DoJ/NSA whistleblower talk</a> (<a href="http://events.ccc.de/congress/2012/Fahrplan/events/5338.en.html" rel="nofollow">blurb</a>).</li>
<li>We talked to Christian Grothoff about <a href="https://gnunet.org/pwnat" rel="nofollow">NAT piercing</a> for Flash Proxy. One of the main deficiencies in the current <a href="https://crypto.stanford.edu/flashproxy/" rel="nofollow">Flash Proxy</a> design is that the censored user needs to be reachable on the Internet (i.e. not behind a firewall or NAT). While we can't expect the flash proxy bridge running in a browser to be able to craft arbitrary packets (required for most NAT piercing tricks), Peter Palfrader pointed out that we *can* expect the Flash Proxy facilitator to be able to send such packets on behalf of each volunteer bridge. Cute trick — wonder if it'll work.</li>
<li>I introduced Harry Halpin (W3C) to David Fifield (Flash Proxy). Web browsers are trying to catch up to Skype in terms of real-time media interactions. That means UDP flows, NAT piercing, link encryption, and more, all in the browser. Flash Proxy could sure make use of all that. And the folks working on the <a href="http://www.webrtc.org/" rel="nofollow">WebRTC</a> specifications could use some broader use cases.</li>
<li>I met several great people from <a href="http://www.bof.nl/" rel="nofollow">Bits of Freedom</a>, the Dutch NGO that is a sort of hybrid EFF/ACLU for the Netherlands. It seems like only a few years ago that we were lamenting that Europe has too few advocacy organizations to challenge bad laws and policies — <a href="http://www.edri.org/issues/privacy/dataretention" rel="nofollow">data retention</a>, <a href="http://www.laquadrature.net/wiki/How_to_act_against_ACTA" rel="nofollow">ACTA</a>, etc. That's changing!</li>
<li>I talked to Linus Nordberg, who runs several fast exits in Sweden as part of <a href="https://dfri.se/" rel="nofollow">DFRI</a> and has been pondering running a bunch of bridges too. The question is: what are the tradeoffs between running both the bridges and exits on the same network (more centralization) vs partitioning them so they run on distinct netblocks?  Counterintuitively, due to the "no more than one node on a given /16" rule in Tor's <a href="https://gitweb.torproject.org/torspec.git?a=blob_plain;hb=HEAD;f=path-spec.txt" rel="nofollow">path selection strategy</a>, centralizing the bridges and exits on the same netblock actually improves safety against some adversaries. My recommendation to him was that having more bridges and <a href="https://lists.torproject.org/pipermail/tor-relays/2012-July/001433.html" rel="nofollow">exits</a> is still better than not, even though the diversity issues remain open and complex research questions.</li>
<li>I also talked to Linus about what we should do with relays whose exit policies only allow ports commonly used for plaintext traffic. Is that a hint that they're set up by jerks to sniff traffic? Or did the operator not even think about that issue? Should we set the BadExit flag for them?  It seems that's a tough arms race for us to win, since they could just choose to exit to a few more ports and suddenly they blend back in. Ultimately I think we need to work harder to establish relationships with all the fast exit relays. We're doing pretty well in terms of knowing the operators of the CCC relays, the Torservers.net relays, the Akamai relays, etc. Will we eventually get to the point where we can cap the bandwidth weights for relays that we haven't met personally? Perhaps we can even bring back the Named or Valid flags for real? In any case, the short-term answer is "send them mail and start a conversation".</li>
<li>I talked to trams about <a href="https://trac.torproject.org/projects/tor/ticket/7008#comment:5" rel="nofollow">sandboxing Flash</a>. It would be great to ship the Tor Browser Bundle with some wrappers that prevent Flash from doing scary things. (Ok, it would be even better to <a href="https://trac.torproject.org/projects/tor/ticket/7681" rel="nofollow">wrap the whole OS</a>, but let's not get hasty.) He has a set of protection wrappers that work on OS X, but his next question is what behaviors to allow? I suggested that to start, we should pick exactly the behaviors Youtube uses — then we'll make a lot of Tor users happier while still not opening the attack surface too much. Next messy steps include "that's nice for OS X users, but what about Windows users?" and "How does this relate to FF17's new <a href="http://support.mozilla.org/en-US/kb/What%20is%20plugin-container" rel="nofollow">plugin-container</a> notion?"</li>
<li>I met with the Wau Holland Foundation board about having WHF be our European coordinator for <a href="https://lists.torproject.org/pipermail/tor-relays/2012-July/001433.html" rel="nofollow">exit relay funding</a>. It's tricky to get everything organized in a way that's compatible with non-profit laws in both the US and Germany, and also in a way where the community understands how the relationships work. We're getting closer.</li>
<li>I met with Andy Isaacson of <a href="https://www.noisebridge.net/" rel="nofollow">Noisebridge</a>, which operates several fast exits in the US under its <a href="http://noisetor.net/" rel="nofollow">Noisetor</a> project. I'd like to sign Noisebridge up to be a US-based coordinator for exit relay funding. But Andy quite reasonably worries that once we start giving Noisetor money for exits, the individual contributions they get to run their exits will disappear. One resolution might be to do one of those "matching funding" deals, where we offer to match every dollar they raise up to some amount.  Ultimately, I hope they work with their community to make a plan that lets them either grow the capacity or diversity of the relays they run, or extend the lifetime of their existing relays.</li>
<li>I talked to bunnie about the <a href="http://www.bunniestudios.com/blog/?p=2686" rel="nofollow">open laptop</a> he's working on. Over in <a href="https://trac.torproject.org/projects/tor/wiki/doc/Torouter" rel="nofollow">Torouter land</a>, we've had a series of experiences where we pick what looks like a fine architecture for a tiny Tor relay. We work with the vendor, help everything go smoothly, and then at the last minute it seems like the vendor goes sideways with some for-profit proprietary alternate plan. :( I really want to live in a world where a fully open platform exists — hardware design and documentation, firmware, device drivers, software, everything. If you can do anything to help bunnie succeed, please do!</li>
</ul>

---
_comments:

<a id="comment-18457"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18457" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2013</p>
    </div>
    <a href="#comment-18457">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18457" class="permalink" rel="bookmark">http://media.ccc.de/browse/co</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://media.ccc.de/browse/congress/2012/29c3-5306-en-the_tor_software_ecosystem_h264.html" rel="nofollow">http://media.ccc.de/browse/congress/2012/29c3-5306-en-the_tor_software_…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-18458"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18458" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 10, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18457" class="permalink" rel="bookmark">http://media.ccc.de/browse/co</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18458">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18458" class="permalink" rel="bookmark">Added into the text above.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Added into the text above. Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-18465"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18465" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 11, 2013</p>
    </div>
    <a href="#comment-18465">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18465" class="permalink" rel="bookmark">Thank you for the heads up.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for the heads up. It was an amazing talk to watch, and it sortof made me dizzy. The ecosystem is so huge. Just tried to learn the basics of how ooni works for 2 weeks, and barely scratching the surface. And all those amazing satellite projects !</p>
<p>And if I may, what was that you wanted to say at the end of your talk that you didn't get time to say ? Like you tried twice or thrice, but the moderator moderated like a boss, but it sortof felt like it was important to say it so you tried to distort space-time to snip it in, but failed to do so because space-time was on the moderator's side ?</p>
<p>(it had to do with the slide   Tormail, TorChat, Advanced Tor, Misc snakeoil ; the snakeoil thing tickled my attention)</p>
<p>You or anybody who followed the 7 hours after-party ? Thx :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-18468"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18468" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 12, 2013</p>
    </div>
    <a href="#comment-18468">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18468" class="permalink" rel="bookmark">I would like to know what</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I would like to know what Jacob was going to say before he was so rudely interrupted by the Internet smear campaign. It went:</p>
<p>Jacob: So, who here uses TorMail? Because after I've given some Tor talks, people who--<br />
Signal Angel: Um, I'd really like to pose a question now. Sorry, hehe.<br />
Jacob: Yep, go for it; you're not talking.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-18511"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18511" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 19, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18468" class="permalink" rel="bookmark">I would like to know what</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18511">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18511" class="permalink" rel="bookmark">I really can&#039;t believe the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I really can't believe the only two questions were so blatantly setup BS from people with other than honest intentions (though, it was awesome to watch Nick's and Jacob's reactions to the citation of Wikipedia as an authorize source!). I was pretty pissed watching the both of them be interrupted, repeatedly, especially because the moderator/sheriff (what a jerk!) cut them both off, repeatedly.</p>
<p>I too wish to hear what Jacob was going to say. Please, Jacob, continue . . .</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-18542"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18542" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 22, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18511" class="permalink" rel="bookmark">I really can&#039;t believe the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18542">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18542" class="permalink" rel="bookmark">Jacob? Are you there? Echo .</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Jacob? Are you there? Echo . . . echo . . . echo . . .</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19285"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19285" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 26, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18511" class="permalink" rel="bookmark">I really can&#039;t believe the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19285">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19285" class="permalink" rel="bookmark">If you check the Tor article</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you check the Tor article on Wikipedia, the info about government funding was added just days before the "question" was asked. There is a massive, global FUD campaign that makes sure imply at public venues that Tor is a honeypot.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
